// SETUP
var monthList = ['januari','februari','maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december'];

var setup = {
bodyLoad:function()
    {
        document.addEventListener("deviceready", setup.deviceReady, false);
    },
    
deviceReady:function()
    {
        document.addEventListener("resume", notifications.checkfortriggers, false);
        setup.init();
    },

checkNotifications:function()
    {
        var id= 2; /* default id, no more than one notification can be set */
        var state = "background";
        /**
        
        /**
         * notificatie afhandeling wanneer deze getriggerd wordt terwijl je de app open hebt staan.
         **/
        window.plugin.notification.local.ontrigger = function (id, state, json) {
            
        navigator.notification.alert("Vergeet u niet om vandaag uw medicatie te bestellen via de Apotheek App.", function(){}, "Bestel herinnering", "OK");
            
        window.plugin.notification.local.cancelAll(function () {
                    // All notifications have been canceled
                });
            
        };
        
        /**
         * functie voor wanneer de app normaal wordt aangeroepen.
         **/
        
        window.plugin.notification.local.isTriggered(id, function (isTriggered)
        {
            if(isTriggered == true)
            {
                //alert('Notification with ID ' + id + ' is triggered: ' + isTriggered);
                navigator.notification.alert("Vergeet u niet om vandaag uw medicatie te bestellen via de Apotheek App.", function(){}, "Bestel herinnering", "OK");
                window.plugin.notification.local.cancelAll(function () {
                // All notifications have been canceled
                });
            }
                            else
                            {
                    //alert('Notification with ID ' + id + ' is triggered: ' + isTriggered);
                        }
        });
    },

init:function()
    {
        dbase.openDB();



        var query = "SELECT * FROM tblTermsAndConditions";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                          
                if(dataset.length == 0)
                {
                    //if no flag is found, load algemene voorwaarden and privacy 
                    $.mobile.changePage("#terms-and-conditions", { transition: "fade", changeHash: false });
                }
                else
                {   
                    //user has already read terms and conditions, continue loading the app
                    setup.continueSetupAndOpenDashboard();
                }
            });
        });
    },
    
continueSetupAndOpenDashboard:function() {
	dbase.checkForPharmacy('dashboard');                    
	deals.retrieve();
	opinion.checkIfFeedbackIsAllowed();	
},
    
registerReadTermsConditionsPrivacy:function()
{
    //alert('flag zetten');
    var deletequery = "DELETE FROM tblTermsAndConditions";
    var insertQuery = "INSERT INTO tblTermsAndConditions (seen) VALUES (?)";
    db.transaction(function(tx, results)
       {
       tx.executeSql(deletequery,[],dbase.succesCB,dbase.errorCB);
       tx.executeSql(insertQuery,[true],dbase.succesCB,dbase.errorCB);
    });
    setup.continueSetupAndOpenDashboard();
},

appIdentifiers:function()
    {
        //alert(device.model);
        //alert(device.platform);
        //alert(device.uuid);
        //alert(device.version);
    },


    
fetchAgbForLogin:function()
    {
        $("#loading").show();
        var query = "SELECT * FROM tblPharmacy";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                          
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Onvoldoende apotheek gegevens beschikbaar.", function(){}, "Foutmelding", "OK");
                }
                else
                {
                    setup.doLogin(dataset.item(0)['agb'], dataset.item(0)['identifier']);
                }
            });
        });
    },
  
getAuthenticationMethod:function()
    {
        var query = "SELECT * FROM tblPharmacy";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                                     
                if(dataset.length == 0)
                {
                    navigator.notification.alert("De authenticatie methode kon niet worden geladen.", function(){}, "Foutmelding", "OK");
                }
                else
                {
                    setup.authenticationMethod(dataset.item(0)['agb']);
                }
            });
        });
    },
    
authenticationMethod:function(agb)
    {
        $('#authenticationTypes').empty();
        $.ajax({
            url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
            type: 'POST',
            data: { authenticationmethod:agb},
            success: function(data)
            {
                $('#authenticationTypes').append($('<label for="authmethod" class="select">Authenticatie methode:</label><select name="authmethod" id="authmethod" data-mini="true">'+data+'</select>'));
                $('#authenticationTypes').trigger('create');
               
                $('#registrationAgb').append($('<input name="agbcode" id="agbcode" value="'+agb+'" type="hidden">'));
            },
            error: function()
            {
               navigator.notification.alert("Er is een onverwachte fout opgetreden bij het ophalen van de authenticatie opties.", function(){}, "Helaas", "OK");
            }
        });
    },

registrationRequest:function()
    {
        var minpasswordlength = 6;
        $("#loadingRegistry").show();
        //var gender = $('input[name="gender"]:selected').val();
        var gender = $( "#gender option:selected" ).val();
        var agb = agbcode.value;
        var voornaam =firstname.value;
        var achternaam = lastname.value;
        var geboortedatum = birthdate.value;
        var straat = street.value;
        var huisnummer = housenumber.value;
        var huisnummertoevoeging = housenumberaddition.value;
        var postcode = zipcode.value;
        var plaats = city.value;
        var email = emailaddress.value;
        var password = wachtwoord.value;
        var repeatpassword = repeatwachtwoord.value;
        var mobiel = phonenumber.value;
        var authenticatie = authmethod.value;

        if(voornaam == "" || achternaam == "" || geboortedatum == "" || straat == "" || huisnummer == "" || postcode == "" || plaats == "" || email == "" || password == "" || mobiel == "" || authenticatie == "")
        {
            $("#loadingRegistry").hide();
            navigator.notification.alert("Om u aan te melden voor deze app dient u alle velden in te vullen.", function(){}, "Melding", "OK");
        }
        else if(password.length <= minpasswordlength)
        {
            $("#loadingRegistry").hide();
            navigator.notification.alert("Het door u ingevoerde wachtwoord dient uit minimaal 6 karakters te bevatten.", function(){}, "Melding", "OK");
        }
        
        else if(password != repeatpassword)
        {
            $("#loadingRegistry").hide();
            navigator.notification.alert("U heeft niet twee keer hetzelfde wachtwoord ingevuld.", function(){}, "Melding", "OK");
        }
        else
        {
            $.ajax({
                   url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                   type: 'POST',
                   data: { newregistration:agb, gender:gender, voornaam:voornaam, achternaam:achternaam, geboortedatum:geboortedatum, straat:straat, huisnummer:huisnummer, toevoeging:huisnummertoevoeging, postcode:postcode, plaats:plaats, email:email, password:password, mobiel:mobiel, authenticatie:authenticatie},
                   success: function(data) {
                   if(data == true)
                   {
                   navigator.notification.alert("U ontvangt een bericht van uw apotheek wanneer uw account is geactiveerd. We streven ernaar om uw aanvraag binnen 1 werkdag te verwerken.", function(){}, "Aanmelding verzonden", "OK");
                   $("#loadingRegistry").hide();
                   $.mobile.changePage("#login-page", { transition: "flip", changeHash: true });
                   }
                   else
                   {
                    $("#loadingRegistry").hide();
                    navigator.notification.alert(data, function(){}, "Melding", "OK");
                   }
                   /*
                    if(data == false)
                    {
                    $("#loadingRegistry").hide();
                    navigator.notification.alert("Er is een fout opgetreden bij het het aanmaken van een account. Controleer uw gegevens en probeer het nogmaals.", function(){}, "Helaas", "OK");
                    }
                    if(data == "error")
                    {
                    $("#loadingRegistry").hide();
                    navigator.notification.alert("Er is een onverwachte fout opgetreden bij het het aanmaken van een account.", function(){}, "Helaas", "OK");
                    }
                    */
                   },
                   error: function()
                   {
                   $("#loadingRegistry").hide();
                   navigator.notification.alert("Er is een onverwachte fout opgetreden bij het het aanmaken van een account..", function(){}, "Helaas", "OK");
                   }
                   });
        }
        //navigator.notification.alert("Uw aanmelding is verzonden naar de apotheek. Voor activatie van uw account dient u persoonlijk langs te gaan bij uw Apotheek.", function(){}, "Aanmelding", "OK");
        
        //$.mobile.changePage("#dashboard-page", { transition: "flip", changeHash: false });
    },

    
doLogin:function(agb,identifier)
    {
        
        var networkState = navigator.connection.type;
        if(networkState == "none")
        {
            navigator.notification.vibrate(1000);
            
            navigator.notification.alert("Om in te kunnen loggen met uw account in de Apotheek App is een internet verbinding vereist. ", function(){}, "Geen internetverbinding", "OK");
        }
        else
        {
        var username = document.getElementById('username');
        var password = document.getElementById('password');
        
        if(username.value == "" || password.value == "" )
        {
            navigator.notification.alert("U heeft geen gebruikersnaam en/of wachtwoord ingevoerd.", function(){}, "Melding", "OK");
            window.scrollTo(0,0);
            $("#loading").hide();
            return;
        }
        
        var deviceType = device.model;
        //var appId = device.uuid;
        var appId = device.uuid+''+identifier;

        $.ajax({
               url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
               type: 'POST',
               data: { doLogin:agb, username:username.value, password:password.value, devicetype:deviceType, appid:appId},
               dataType:'json',
               success: function(data) {
               //alert(JSON.stringify(data));
                    switch (data["Result"]) {
                        case "Success":
                            navigator.notification.alert("Uw gebruikersaccount is succesvol gekoppeld aan deze app.", function(){}, "Aanmelding", "OK");
                            window.scrollTo(0,0);
                            dbase.insertKey(data["ApiKey"]);
                        break;
               
                        case "NoAccount":
                            $("#loading").hide();
                            navigator.notification.alert("Er is geen gebruikersaccount gevonden met de door u opgegeven gebruikersnaam en wachtwoord", function(){}, "Helaas", "OK");
                            window.scrollTo(0,0);
               
                            return;
                        break;
               
                        case "PasswordError":
                            $("#loading").hide();
                            navigator.notification.alert("Er is geen gebruikersaccount gevonden met de door u opgegeven gebruikersnaam en wachtwoord", function(){}, "Helaas", "OK");
                            window.scrollTo(0,0);
                            return;
                        break;
               
                        case "VerificationCodeNeededSms":
                            $("#loading").hide();
                            navigator.notification.alert("Er is een verificatiecode per sms naar u verzonden om de authenticatie met de app af te ronden.", function(){}, "Verificatiecode verzonden", "OK");
               
                                $("#ssimtoken").val(data["SSIMToken"]);
                                $("#agb").val(agb);
                                $("#identifier").val(identifier);
               
               
                            $.mobile.changePage("#verificationcode-page", { transition: "flip", changeHash: true });
                            return;

                        break;
               
                        case "VerificationCodeNeededEmail":
                            $("#loading").hide();
                            navigator.notification.alert("Er is een verificatiecode per e-mail naar u verzonden om de authenticatie met de app af te ronden.", function(){}, "Verificatiecode verzonden", "OK");
               
                            $("#ssimtoken").val(data["SSIMToken"]);
                            $("#agb").val(agb);
                            $("#identifier").val(identifier);
               
                            $.mobile.changePage("#verificationcode-page", { transition: "flip", changeHash: true });
                            return;
                        break;
               
                        case "UnknownError":
                            $("#loading").hide();
                            navigator.notification.alert("Er is een onbekende fout opgetreden. Probeer het later nogmaals.", function(){}, "Melding", "OK");
                            window.scrollTo(0,0);
                            return;
                        break;
               }
               },
               error: function()
               {
                    $("#loading").hide();
                    navigator.notification.alert("Het is niet gelukt om uw login verzoek af te handelen. Probeer het later nogmaals", function(){}, "Helaas", "OK");
               }
                });
        }
    },

sendVerificationData:function()
    {
        var networkState = navigator.connection.type;
        if(networkState == "none")
        {
            navigator.notification.vibrate(1000);
            
            navigator.notification.alert("Om in te kunnen loggen met uw account in de Apotheek App is een internet verbinding vereist. ", function(){}, "Geen internetverbinding", "OK");
            //$.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: false });
        }
        else
        {
        $("#loadingverify").show();
        var agb = document.getElementById('agb');
        var identifier = document.getElementById('identifier');
        var ssimtoken = document.getElementById('ssimtoken');
        var code = document.getElementById('verificationcode');
        var deviceType = device.model;
        //var appId = device.uuid;
            
        var appId = device.uuid+''+identifier.value;
        
        $.ajax({
               url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
               type: 'POST',
               data: { verificationcode:code.value, ssimtoken:ssimtoken.value, agb:agb.value, devicetype:deviceType, appid:appId},
               dataType:'json',
               success: function(data) {
               switch (data["Result"])
                {
                    case "Success":
                    navigator.notification.alert("Uw gebruikersaccount is succesvol gekoppeld aan deze app.", function(){}, "Aanmelding", "OK");
                    window.scrollTo(0,0);
               
                    dbase.insertKey(data["ApiKey"]);
                    break;
               
                    case "Error":
                    $("#loadingverify").hide();
              
                    navigator.notification.alert("De door u opgegeven verificatiecode is incorrect. Probeer het nogmaals", function(){}, "Helaas", "OK");
                    window.scrollTo(0,0);
                    return;
                    break;
                }
               },
               error: function()
               {
                    $("#loadingverify").hide();
                    navigator.notification.alert("Het is niet gelukt om de verificatiecode te valideren. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
               }
               });
        }
    },

pharmacySelect:function(messagetype)
    {
        if(messagetype == 'pharmacyselect')
        {
            $('#selectExplanation').empty();
            $('#selectExplanation').append('Om uw actuele medicatie in te kunnen zien dient u uw apotheek te selecteren uit onderstaande lijst. Vervolgens kunt u inloggen met dezelfde gegevens als u gebruikt op de website van de apotheek.');
        }
        if(messagetype == 'webshoppharmacy')
        {
            $('#selectExplanation').empty();
            $('#selectExplanation').append('Selecteer hier onder de apotheek waar u uw bestelling wenst op te halen.');
        }
        if(messagetype == 'pharmacyinfo')
        {
            $('#selectExplanation').empty();
            $('#selectExplanation').append('Selecteer uit onderstaande lijst uw apotheek.');
        }
        
        var networkState = navigator.connection.type;
        if(networkState == "none")
        {
            navigator.notification.vibrate(1000);
            
           navigator.notification.alert("Een internet verbinding is vereist om voor u een lijst met beschikbare apotheken te tonen ", function(){}, "Geen internetverbinding", "OK");
        }
        else
        {
        $('ul#pharmacyList').empty();
        var getList = 'true'; //fixed variable for now, make dynamic if needed for later purpose.
        $.ajax({
               url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
               type: 'POST',
               data: { getPharmacyList:getList, type:messagetype},
               success: function(data) {
                $('ul#pharmacyList').append(data).listview('refresh');
               },
               error: function()
               {
                navigator.notification.alert("Het is niet gelukt om de lijst met beschikbare apotheken op te halen.", function(){}, "Helaas", "OK");
               }
               });
        
        $.mobile.changePage("#selectPharmacy-page", { transition: "slideup", changeHash: true });
        }
    },

confirmPharmacy:function(agb,pharmacy,type)
    {
        navigator.notification.confirm(
            'Wilt u '+pharmacy+' opslaan als uw apotheek?',
            function(button) {
                if (button == 1)
                {
                    setup.savePharmacy(agb,pharmacy, type);
                }
            },
            'Bevestig uw keuze',
            ['Ja','Nee']
        );
        
    },

savePharmacy:function(agb,pharmacy,type)
    {
        var uniqueidentifier = Math.floor(100000 + Math.random() * 900000);
        var deletequery = "DELETE FROM tblPharmacy";
//        var insertQuery = "INSERT INTO tblPharmacy (agb, pharmacy) VALUES (?, ?)";
        var insertQuery = "INSERT INTO tblPharmacy (agb, pharmacy, identifier) VALUES (?, ?, ?)";
        
        db.transaction(function(tx, results)
                       {
                       tx.executeSql(deletequery,[],dbase.succesCB,dbase.errorCB);
                       //tx.executeSql(insertQuery,[agb, pharmacy],dbase.succesCB,dbase.errorCB);
                       tx.executeSql(insertQuery,[agb, pharmacy,uniqueidentifier],dbase.succesCB,dbase.errorCB);
                       });
        if(type == 'webshoppharmacy')
        {
                //$.mobile.changePage("#persoonsgegevens-page", { transition: "slide", changeHash: false });
            webshop.getPersonalData();
            opinion.checkIfFeedbackIsAllowed();
        }
        if(type == 'pharmacyinfo')
        {
            //$.mobile.changePage("#pharmacy-page", { transition: "slide", changeHash: false });
            app.pharmacyDetails();
            opinion.checkIfFeedbackIsAllowed();
        }
        if(type == 'pharmacyselect')
        {
            //recept.checkIfReceptNumberIsAllowed();
            $.mobile.changePage("#login-page", { transition: "slide", changeHash: true });
            opinion.checkIfFeedbackIsAllowed();
        }
    },
   
openURL:function(urlString)
    {
        myURL = encodeURI(urlString);
        window.open(myURL, "_system","location=yes");
        
    }
};

var app = {

getPharmacyName:function()
    {
        var query = "SELECT * FROM tblPharmacy";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Onvoldoende apotheek gegevens beschikbaar.", function(){}, "Foutmelding", "OK");
                          
                    $.mobile.changePage("#selectPharmacy-page", { transition: "slideup", changeHash: true });
                }
                else
                {
                    $('#pharmacyName').append(dataset.item(0)['pharmacy']);
                    apotheeknaam = dataset.item(0)['pharmacy'];
                    apotheekid = dataset.item(0)['agb'];
                }
            });
        });
    },
    
    
getPharmacyDetails:function()
    {
        var networkState = navigator.connection.type;
        if(networkState == "none")
        {
            navigator.notification.vibrate(1000);
            
            navigator.notification.alert("Om herhaalmedicatie te kunnen bestellen met de Apotheek App is een internet verbinding vereist. ", function(){}, "Geen internetverbinding", "OK");
            $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
        }
        $("#loading").show();
        var query = "SELECT * FROM tblPharmacy";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                                     
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Onvoldoende apotheek gegevens beschikbaar.", function(){}, "Foutmelding", "OK");
                          
                    $.mobile.changePage("#selectPharmacy-page", { transition: "slideup", changeHash: true });
                }
                else
                {
                    $.ajax({
                           url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                           type: 'POST',
                            data: { getPharmacyDetails:dataset.item(0)['agb']},
                            success: function(data) {
                           
                           $('#pharmacyInfo').append(data);
                           

                           $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
                            },
                            error: function()
                            {
                                navigator.notification.alert("Het is niet gelukt om de apotheek details op te halen", function(){}, "Helaas", "OK");
                            }
                            });
                }
            });
        });
    },
    
    
pullPharmacyAgb:function(getmedication)
    {
        var query = "SELECT * FROM tblPharmacy";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Onvoldoende apotheek gegevens beschikbaar.", function(){}, "Foutmelding", "OK");
                }
                else
                {
                          
                    if(getmedication == true)
                    {
                          app.pullApiKey(getmedication,dataset.item(0)['agb'],dataset.item(0)['identifier']);
                    }
                    else
                    {
                          app.sendToestemming(dataset.item(0)['agb']);
                          //navigator.notification.alert("Excuses, er is een onbekende fout opgetreden.", function(){}, "Foutmelding", "OK");
                    }
                }
            });
        });

        
    },

sendToestemming:function(agb)
    {
        var voornaam = document.getElementById('voornaam');
        var tussenvoegsel = document.getElementById('tussenvoegsel');
        var achternaam = document.getElementById('achternaam');
        var geboortedatum = document.getElementById('geboortedatum');
        var toestemmingVraag = document.getElementById('toestemmingVraag');
        
        $.ajax({
               url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
               type: 'POST',
               data: { toestemming:toestemmingVraag.value, voornaam:voornaam.value, tussenvoegsel:tussenvoegsel.value, achternaam:achternaam.value, geboortedatum:geboortedatum.value, agb:agb},
               success: function(data) {
                    if(data == true)
                    {
                        $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
                        navigator.notification.alert("Uw toestemmingsverklaring is verzonden naar de apotheek.", function(){}, "Verzonden", "OK");
                    }
                },
               error: function()
               {
               navigator.notification.alert("Het is niet gelukt om de apotheek details op te halen", function(){}, "Melding", "OK");
               }
               });
        
    },
    

pullApiKey:function(getmedication,agb,identifier)
    {
        var query = "SELECT * FROM tblhhrApi";
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                   navigator.notification.alert("Er zijn geen actieve medicatieregels gevonden.", function(){}, "Foutmelding", "OK");
                }
                else
                {
                    
                    if(getmedication == true)
                    {
                          app.getMedication(agb,dataset.item(0)['apiKey'],identifier);
                    }
                    else
                    {
                        navigator.notification.alert("Onvoldoende apotheek gegevens beschikbaar.", function(){}, "Foutmelding", "OK");
                    }
                }
            });
        });
    },
    
prepareMedicationData:function()
    {
        $("#loaderMedication").show();
        
        var getmedication = true;
        app.pullPharmacyAgb(getmedication);
    },

getMedication:function(agb,apikey,identifier)
    {
        $("#resetPincodeOption").show();
        var networkState = navigator.connection.type;
        if(networkState == "none")
        {
            navigator.notification.vibrate(1000);
            
            navigator.notification.alert("Om herhaalmedicatie te kunnen bestellen met de Apotheek App is een internet verbinding vereist. ", function(){}, "Geen internetverbinding", "OK");
            $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
        }
        else
        {
            //var appid = device.uuid;
            if(identifier == null)
            {
                identifier = '';
            }
            
            var appid = device.uuid+''+identifier;
            var deviceType = device.model;

            dbase.dropTable();
                $.ajax({
                       url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                       type: 'POST',
                       dataType:'json',
                       data: { getmedication:agb, appid:appid, devicetype:deviceType, apikey:apikey},
                       success: function(data) {
                       
                       //data["Medication"] = {MedicationId:1, MedicationName:'Paracetamol', StartDate:'10/10/2016', EndDate:'10/10/2018', PrescriberName:'Janssen',  UsageText:'1xdaags 1 tablet'};
                       if(data["Medication"] == undefined)
                       {
                            $("#loadMed").hide();
                       
							navigator.notification.alert("Er is geen actieve medicatie voor u gevonden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
                       
							$.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true }); 
							
							//voorkomt dat onderstaande code ook nog wordt uitgevoerd, dit omdat die notification.alert asynchrone lijkt
							return;            
                       }
                       if(data["Medication"].length == undefined)
                       {
                            dbase.insertMedication(data["Medication"]["MedicationId"], data["Medication"]["MedicationName"],data["Medication"]["StartDate"],data["Medication"]["EndDate"],data["Medication"]["PrescriberName"],data["Medication"]["UsageText"]);
                            $("#loadMed").hide();
                       
                            $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
                       }
                        for (var i = 0; i < data["Medication"].length; i++)
                        {
                       
                            var MedicationId = data["Medication"][i]["MedicationId"];
                            var MedicationName = data["Medication"][i]["MedicationName"];
                       
                            var startDate = data["Medication"][i]["StartDate"];
                            var endDate = data["Medication"][i]["EndDate"];
                            var prescriberName = data["Medication"][i]["PrescriberName"];
                            var usageText = data["Medication"][i]["UsageText"];
                       
                            dbase.insertMedication(MedicationId, MedicationName, startDate, endDate,prescriberName,usageText);

                       
                            //dbase.insertMedication(MedicationId, MedicationName);
                       
                            if(i == data["Medication"].length -1)
                            {
                                $("#loadMed").hide();
              
                                $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
                            }
                        }
                       },
                       error: function()
                       {
                        navigator.notification.alert("Het is niet gelukt om de apotheek details op te halen", function(){}, "Melding", "OK");
                       }
               });
        }
    },
    
showMedication:function()
    {
        recept.checkIfReceptNumberIsAllowed();
        var query = "SELECT * FROM tblMedication";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    /**
                     * check for connected pharmacy
                     **/
                    dbase.checkForPharmacy('pharmacyselect');
                }
                else
                {
                    pin.CodeExists();
                    $("#loaderMedication").hide();
                    $('#medicationList').empty();
                    //$('#medicationList').append($('<ul id="listUl" style="margin-left:-17px; margin-right: -17px; padding:0px;" data-role="listview" data-theme="d" data-split-theme="d" data-icon="false" data-inset="true">'));
                    $('#medicationList').append($('<ul data-role="listview" id="listUl-new" data-icon="false">'));
                    $('#medicationList').trigger('create');
                    
                    for (var i = 0, item =null; i < dataset.length; i++)
                    {
                          item = dataset.item(i);
                          app.categorizeMedicationList(item);
                    }
                }
            });
        });
        add.checkForAddedMedication();
    },

categorizeMedicationList:function(item)
    {
        /**
         * Check if medicationID is found in removed Medication table
         **/
        var checkquery = "SELECT * FROM tblRemovedMedication WHERE medicationID = '"+item['medicationID']+"'";
        db.transaction(function(tx)
        {
            tx.executeSql(checkquery,[],function(tx, result)
            {
                resultset = result.rows;
                if(resultset.length == 0)
                {
                    var icon = 'fa-cart-plus';
                    var icontext = 'Bestellen';
                    var status = 'getOrderDate';
                    if(item['status'] == 'yes')
                    {
                        var icon = 'fa-cart-arrow-down ';
                        var icontext = 'Toegevoegd';
                        var status = 'removefromcart';
                    }
                          
                    $('ul#listUl-new').append('<li id="'+item['medicationID']+'" class="MedName"><strong>'+item['medicationName']+'</strong><br /><em>'+item['usageText']+'</em><div class="ordericons-new"><div class="iconfloat" onclick="app.medicationDetails(\''+item['medicationID']+'\');""><i class="fa fa-info-circle"></i> Informatie</div><div class="iconfloat" onclick="intakes.intakeSchedule(\''+item['medicationID']+'\')"><i class="fa fa-calendar"></i> Innamen</div><div class="iconfloat" style="border-right: 0px solid #FFF; width: 100px;" onclick="app.'+status+'(\''+item['medicationID']+'\');"><i class="fa '+icon+'"></i> '+icontext+'</div><div class="clearer"></div></div></li>').listview('refresh');
                }
                else
                {
                    /*
                        $('ul#overviewList').append('<li class="grayLi"><div id="'+item['medicationID']+'" class="grayName" onclick="app.QuitedMedicationDetails(\''+item['medicationID']+'\');">'+item['medicationName']+'</div><div class="grayMedOptions" onclick="app.restoreMedication(\''+item['medicationID']+'\')"><i class="fa fa-check-square-o"></i>Ik gebruik dit medicijn weer</div></li>').listview('refresh');
                    
                    */
                    $('ul#listUl-new').append('<li id="'+item['medicationID']+'" class="MedNameGray"><strong>'+item['medicationName']+'</strong><br /><em>'+item['usageText']+'</em><div class="ordericons-new"><div class="iconfloat" onclick="feedback.restoreMedication(\''+item['medicationID']+'\')" style="border-right: 0px solid #FFF; text-align: center; width:100%;"><i class="fa fa-check-square-o"></i> Ik gebruik dit geneesmiddel weer</div><div class="clearer"></div></div></li>').listview('refresh');
                                     
                }
            });
        });
    },


medicationDetails:function(medicationID)
    {
        var query = "SELECT * FROM tblMedication WHERE medicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                var start = new Date(dataset.item(0)["startDate"]);
                                     
                var startday = ('0' + start.getUTCDate()).slice(-2);
                var startmonth = start.getUTCMonth()+1;
                var startmonth = ('0' + startmonth).slice(-2);
                var startyear = start.getUTCFullYear();
                var startdate = startday+'-'+startmonth+'-'+startyear;
                                     
                var end = new Date(dataset.item(0)["endDate"]);
                                     
                var endday = ('0' + end.getUTCDate()).slice(-2);
                var endmonth = end.getUTCMonth()+1;
                var endmonth = ('0' + endmonth).slice(-2);
                var endyear = end.getUTCFullYear();
                var enddate = endday+'-'+endmonth+'-'+endyear;
                //alert(startdate);
                                     
                $('#detailName').empty();
                $('#detailName').append(dataset.item(0)["medicationName"]);
                var fullmedicationname = dataset.item(0)["medicationName"];
                                     
                var linksearch = fullmedicationname.split(" ",1);
                                     
                $('#detailsContainer').empty();
                $('#editOptions').empty();
                $('#newPrescription').hide();
                          
                $('#detailsContainer').append('<div class="detailfield">Startdatum</div><div class="detailvalue">'+startdate+'</div><div class="clearer"></div>');
                                     
                $('#detailsContainer').append('<div class="detailfield">Einddatum</div><div class="detailvalue">'+enddate+'</div><div class="clearer"></div>');
                                     
                $('#detailsContainer').append('<div class="detailfield">Voorschrijver</div><div class="detailvalue">'+dataset.item(0)["prescriberName"]+'</div><div class="clearer"></div>');
                                     
                $('#detailsContainer').append('<div class="detailfield">Voorschrift</div><div class="detailvalue">'+dataset.item(0)["usageText"]+'</div><div class="clearer"></div>');
                                     
                $('#detailsContainer').append('<div style="margin-top: 25px; margin-bottom: 15px; text-align:center;"><a href="#" onclick=\'window.open("http://www.apotheek.nl/medicijnen#search/medicine/'+linksearch+'","_system","location=yes","closebuttoncaption=Terug")\'>Lees meer informatie op apotheek.nl</a></div><div class="clearer"></div>');
                          
                feedback.GetUpdatedPrescription(medicationID);
            });
        });
        
        $.mobile.changePage("#medication-detail-page", { transition: "pop", changeHash: true });
    },

    
addtocart:function(id)
    {
        var updateMedStatus = "UPDATE tblMedication SET status = 'yes' WHERE medicationID = '"+id+"'";
        
        db.transaction(function(tx)
        {
           tx.executeSql(updateMedStatus);
            app.showMedication();
        });
    },
    
removefromcart:function(id)
    {
        var updateMedStatus = "UPDATE tblMedication SET status = 'no' WHERE medicationID = '"+id+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(updateMedStatus);
            app.showMedication();
        });
    },
 
confirmOrder:function()
    {
        /*
        navigator.notification.confirm(
        'Wilt u de geselecteerde medicijnen bestellen bij de apotheek?',
        function(button) {
        if (button == 1)
        {
            app.orderMedication();
        }
        
        },
            'Bestellen',
            ['Ja','Nee']
        );
         */
    },
    
orderMedication:function()
    {
        var query = "SELECT * FROM tblMedication WHERE status = 'yes'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                          
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Om te kunnen bestellen dient u één of meerdere medicijnen te selecteren.", function(){}, "Geen medicatie toegevoegd", "OK");
                }
                else
                {
                    $('#orderOverviewList').empty();
                    for (var i = 0, item =null; i < dataset.length; i++)
                    {
                        item = dataset.item(i);
                          //medicationID TEXT, medicationName
                          var counter = i+1;
                          
                          if(counter == dataset.length)
                          {
                            $('#orderOverviewList').append('<div class="OrderItem">'+item.medicationName+'</div>');
                          }
                          else
                          {
                            $('#orderOverviewList').append('<div class="OrderItem" style="border-bottom: 1px solid #009435;">'+item.medicationName+'</div>');
                          }
                    }
                          
                    $.mobile.changePage("#medicationorder-page", { transition: "slide", changeHash: true });
                }
            });
        });
    },

getOrderDate:function(medicationID)
    {
        var today = new Date();
        var dd = today.getDate();
        
        var yyyy = today.getFullYear();
        if(dd<10){dd='0'+dd}
        
        var month=new Array();
        month[0]="januari";
        month[1]="februari";
        month[2]="maart";
        month[3]="april";
        month[4]="mei";
        month[5]="juni";
        month[6]="juli";
        month[7]="augustus";
        month[8]="september";
        month[9]="oktober";
        month[10]="november";
        month[11]="december";
        
        var mm = month[today.getMonth()];
        var today = dd+' '+mm+' '+yyyy;
        
        var query = "SELECT * FROM tblHistory WHERE medicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                          
                if(dataset.length == 0)
                {
                    app.addtocart(medicationID);
                }
                else
                {
                    if(dataset.item(0)['orderDate'] == today)
                    {
                        navigator.notification.alert("U heeft dit product vandaag al besteld.", function(){}, "Product al besteld", "OK");
                    }
                    else
                    {
                        app.addtocart(medicationID);
                    }
                }
            });
        });
    },
    
insertHistory:function(medicationID,medicationName)
    {
        var today = new Date();
        var dd = today.getDate();
        
        var yyyy = today.getFullYear();
        if(dd<10){dd='0'+dd}
        
        var month=new Array();
        month[0]="januari";
        month[1]="februari";
        month[2]="maart";
        month[3]="april";
        month[4]="mei";
        month[5]="juni";
        month[6]="juli";
        month[7]="augustus";
        month[8]="september";
        month[9]="oktober";
        month[10]="november";
        month[11]="december";
        
        var mm = month[today.getMonth()];
        var today = dd+' '+mm+' '+yyyy;

        var insertKey = "INSERT INTO tblHistory (medicationID, orderDate,historyName) VALUES (?,?,?)";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(insertKey,[medicationID, today,medicationName],dbase.succesCB,dbase.errorCB);
        });
         
    },
    
rebuildMedicationOrder:function()
    {
        var query = "SELECT * FROM tblMedication WHERE status = 'yes'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                          
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Om te kunnen bestellen dient u één of meerdere medicijnen te selecteren.", function(){}, "Geen medicatie toegevoegd", "OK");
                }
                else
                {
                    app.sendOrderMedication(dataset);
                }
            });
        });
    },

getOrderHistory:function()
    {
        $('#historyContainer').empty();
        var query = "SELECT tblHistory.*, tblMedication.medicationName as medID FROM tblHistory LEFT JOIN tblMedication ON tblMedication.medicationID LIKE tblHistory.medicationID ORDER BY orderDate";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                
                if(dataset.length == 0)
                {
                    navigator.notification.alert("U heeft nog geen medicatie besteld via de App dat getoond kan worden in het overzicht.", function(){}, "Geen medicatie gevonden", "OK");
                }
                else
                {
                    for (var i = 0, item =null; i < dataset.length; i++)
                    {
                        item = dataset.item(i);
                          
                        dataset = result.rows;
                        var date = new Date(item.orderDate);
                        var day = date.getDate();
                        var month = date.getMonth()+1;
                        var year = date.getFullYear();
                                          
                        if(item.historyName == undefined)
                        {
                            var medname = dataset.item(0)['medID'];
                            $('#historyContainer').append('<div class="historyDate">'+day+'-'+month+'-'+year+'</div><div class="historyName">'+medname+'</div><div class="clearer"></div>');
                        }
                        else {
                            var medname = item.historyName;
                            $('#historyContainer').append('<div class="historyDate">'+day+'-'+month+'-'+year+'</div><div class="historyName">'+medname+'</div><div class="clearer"></div>');
                        }
                    }
                          
                    $.mobile.changePage("#medicationhistory-page", { transition: "pop", changeHash: true });
                }
                
            });
        });
    },
    
sendOrderMedication:function(medication)
    {
        $("#loadingOrderMedication").show();
        var medicationId = new Array();
        var medicationName = new Array();
        
        
        for (var i = 0, item =null; i < medication.length; i++)
        {
            item = medication.item(i);
            
            medicationId[i] = item['medicationID'];
            medicationName[i]= item['medicationName'];
            
            app.insertHistory(medicationId[i],medicationName[i]);
        }
        
        var query = "SELECT agb, apiKey, identifier FROM tblPharmacy CROSS JOIN tblhhrApi";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                                     
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Onvoldoende gegevens om te kunnen authenticeren.", function(){}, "Authenticatie error", "OK");
                }
                else
                {
                    var agb = dataset.item(0)['agb'];
                    var apiKey = dataset.item(0)['apiKey'];
                    var identifier = dataset.item(0)['identifier'];
                    var deviceType = device.model;
                    
                          if(identifier == null)
                          {
                          identifier = '';
                          }
                          
                          var appId = device.uuid+''+identifier;
                    //var appId = device.uuid;
                                     
                    $.ajax({
                        url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                        type: 'POST',
                        timeout: 2*60*60*1000,
                        data: { orderMedication:medicationId, medicationName:medicationName, agb:agb, appid:appId, devicetype:deviceType, apikey:apiKey},
                        success: function(data) {
                           
                           if(data == "Success")
                           {
                                for (var i = 0, item =null; i < medication.length; i++)
                                {
                                    item = medication.item(i);
                                    app.removefromcart(item['medicationID']);
                                }
                           
                                //navigator.notification.vibrate(1000);
                           
                           
                           /*
                           navigator.notification.confirm(
                                                          'Uw bestelling is verzonden. Wilt u een bestelherinnering instellen?',
                                                          function(button) {
                                                          if (button == 1)
                                                          {
                                                          var x = 3; //or whatever offset
                                                          var CurrentDate = new Date();
                                                          CurrentDate.setMonth(CurrentDate.getMonth() + x);
                                                          
                                                          var day = CurrentDate.getDate();
                                                          var month = CurrentDate.getMonth() + 1;
                                                          var year = CurrentDate.getFullYear();
                                                          var setorderdate = year+'-'+month+'-'+day;
                                                          
                                                          $("#reminderdate").val(setorderdate);
                                                            $.mobile.changePage("#notification-page", { transition: "pop", changeHash: false });
                                                            $("#loadingOrderMedication").hide();
                                                          }
                                                          if (button == 2)
                                                          {
                                                          $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: false });
                                                          $("#loadingOrderMedication").hide();
                                                          }
                                                          },
                                                          'Verzonden',
                                                          ['Ja','Nee']
                                                          );
                           */
                                navigator.notification.alert("Uw bestelling is verzonden naar de apotheek", function(){}, "Verzonden", "OK");
                                $("#loadingOrderMedication").hide();
                           }
                           else
                           {

                           navigator.notification.alert("Er is iets misgegaan bij het bestellen van uw medicatie. Probeer het later nogmaals..", function(){}, "Foutmelding", "OK");
                           }
                        },
                        error: function()
                        {

                           navigator.notification.alert("Het is niet gelukt om uw bestelling te versturen naar de apotheek. Probeer het later nogmaals. ", function(){}, "Helaas", "OK");
                           $("#loadingOrderMedication").hide();
                        }
                        });
                }
            });
        });
    },

setReminder:function()
    {
        var reminderdate = document.getElementById('reminderdate');
        var reminder = new Date(reminderdate.value+'T12:30:00');
        //var now                  = new Date().getTime(),
        //reminder = new Date(now + 20*1000);
        
        window.plugin.notification.local.add({
                                             id:      2,
                                             title:   '',
                                             message: 'Vergeet u niet om vandaag uw medicatie te bestellen via de Apotheek App.',
                                             repeat:  'weekly',
                                             badge: 1,
                                             date:    reminder
                                             });

        
        navigator.notification.alert("U ontvangt op "+reminderdate.value+" een bestelherinnering van de Apotheek App", function(){}, "Herinnering ingesteld", "OK");
        $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
    },
    
pharmacyDetails:function()
    {
        var networkState = navigator.connection.type;
        if(networkState == "none")
        {
            navigator.notification.vibrate(1000);
            
            navigator.notification.alert("Een internet verbinding is vereist om de gegevens van uw apotheek te kunnen bekijken.", function(){}, "Geen internetverbinding", "OK");
            
            $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
        }
        
        //$("#loading").show();
        var query = "SELECT * FROM tblPharmacy";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                                     
                if(dataset.length == 0)
                {
                    /*
                    navigator.notification.alert("Onvoldoende apotheek gegevens beschikbaar.", function(){}, "Foutmelding", "OK");
                          
                    $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: false });
                     */
                
                    dbase.checkForPharmacy('pharmacyinfo');
                }
                else
                {
                    $.ajax({
                    url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                    type: 'POST',
                    data: { getPharmacyDetails:dataset.item(0)['agb']},
                    success: function(data) {
                        $('#pharmacyInfo').empty();
                        $('#pharmacyInfo').append(data);
                        $.mobile.changePage("#pharmacy-page", { transition: "slide", changeHash: true });
                    },
                    error: function()
                    {
                        navigator.notification.alert("Het is niet gelukt om de apotheek details op te halen", function(){}, "Helaas", "OK");
                    }
                    });
                }
            });
        });
    },
showMedicationName:function(medicationID, showType)
    {
        /**
         * reusable function for getting and parsing pharmacy name.
         **/
        var query = "SELECT * FROM tblMedication WHERE medicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    //alert('deze functie moet nog geimplementeerd worden.'+medicationID);
                    add.showAddedMedicationName(medicationID, showType);
                }
                else
                {
                    $('#'+showType+'').empty();
                    $('#'+showType+'').append(dataset.item(0)["medicationName"]);
                }
            });
        });
    },
    
showUsageText:function(medicationID, showType)
    {
        /**
         * reusable function for getting and parsing usage text name.
         **/
        var query = "SELECT * FROM tblMedication WHERE medicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    //alert('deze functie moet nog geimplementeerd worden.');
                    add.showAddedUsageText(medicationID, showType);
                }
                else
                {
                    $('#'+showType+'').empty();
                    $('#'+showType+'').append('<em>'+dataset.item(0)["usageText"]+'</em>');
                }
            });
        });
    },
    insertMedicationName:function(medicationID, showType)
    {
        /**
         * reusable function for getting and parsing pharmacy name.
         **/
        var query = "SELECT * FROM tblMedication WHERE medicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    //alert('deze functie moet nog geimplementeerd worden.');
                    add.insertaddedMedicationName(medicationID, showType);
                }
                else
                {
                    $('#'+showType+'').val(dataset.item(0)["medicationName"]);
                }
            });
        });
    },

};

var webshop = {
getArticles:function(searchvalue)
    {
        $('#articlelist').empty();
        //var searchvalue = document.getElementById('search');
        var article = "list";
        
        var networkState = navigator.connection.type;
        if(networkState == "none")
        {
            navigator.notification.vibrate(1000);
            
            navigator.notification.alert("Een internet verbinding is vereist om de gegevens van uw apotheek te kunnen bekijken.", function(){}, "Geen internetverbinding", "OK");
            
            $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
        }
        else
        {
            var basketArray = new Array();
            var query = "SELECT * FROM tblShoppingCart";
            
            db.transaction(function(tx)
            {
                tx.executeSql(query,[],function(tx, result)
                {
                    dataset = result.rows;
                                         
                    if(dataset.length == 0)
                    {
                        //webshop.getArticleList(article, basketArray,searchvalue);
                        webshop.searchArticles(article, basketArray,searchvalue);
                    }
                    else
                    {
                        
                        for (var i = 0, item =null; i < dataset.length; i++)
                        {
                            item = dataset.item(i);
                            basketArray[i] = item['articleID'];
                        }
                        
                        //webshop.getArticleList(article, basketArray, searchvalue);
                        webshop.searchArticles(article, basketArray, searchvalue);
                    }
                });
            });
            
        }
    },


searchArticles:function(article, basketArray, searchvalue)
    {
        $.ajax({
               url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
               type: 'POST',
               data: { searcharticles:searchvalue,inBasket:basketArray},
               success: function(data) {
               if(data == 'no results')
               {
                    $('#articlelist').append('<br /><br />We hebben geen producten kunnen vinden die voldoen aan uw zoekopdracht. <br /><br />Probeert u het nogmaals met een aangepaste zoekopdracht.');
               }
               else
               {
                    $('#articlelist').append($('<ul id="webshopUl" style="margin-left:-17px; margin-right: -17px; padding:0px;" data-role="listview" data-theme="d" data-split-theme="d" data-icon="false" data-inset="true">'));
                    $('#articlelist').trigger('create');
                    $('ul#webshopUl').append(data).listview('refresh');
               }
               },
               error: function()
               {
               navigator.notification.alert("Het is niet gelukt om de apotheek details op te halen", function(){}, "Helaas", "OK");
               }
               });
    },

    
getArticleList:function(article, basketArray)
    {
       
        $.ajax({
               url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
               type: 'POST',
               data: { getarticles:article,inBasket:basketArray},
               success: function(data) {
               $('#articlelist').append($('<ul id="webshopUl" style="margin-left:-17px; margin-right: -17px; padding:0px;" data-role="listview" data-theme="d" data-split-theme="d" data-icon="false" data-inset="true" data-filter="true" data-filter-placeholder="Zoek een artikel">'));
               
               $('#articlelist').trigger('create');
               
               
               $('ul#webshopUl').append(data).listview('refresh');
               
               },
               error: function()
               {
                    navigator.notification.alert("Het is niet gelukt om de apotheek details op te halen", function(){}, "Helaas", "OK");
               }
               });
    },
    
addToShoppingCart:function(articleID)
    {
        var search = document.getElementById('search');
        var insertProduct = "INSERT INTO tblShoppingCart (articleID,aantal) VALUES (?,?)";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(insertProduct,[articleID,'1'],dbase.succesCB,dbase.errorCB);
            if(search.value != "")
            {
                webshop.getArticles(search.value);
            }
                    
            navigator.notification.alert("Het product is toegevoegd aan de bestellijst", function(){}, "Toegevoegd", "OK");
        });
    },

dashboardAddToShoppingCart:function(articleID)
    {
        var search = document.getElementById('search');
        var insertProduct = "INSERT INTO tblShoppingCart (articleID,aantal) VALUES (?,?)";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(insertProduct,[articleID,'1'],dbase.succesCB,dbase.errorCB);
            if(search.value != "")
            {
                webshop.getArticles(search.value);
            }
            webshop.getShoppingList();
                       
            navigator.notification.alert("Het product is toegevoegd aan de bestellijst", function(){}, "Toegevoegd", "OK");
            $.mobile.changePage("#shoppingcart-page", { transition: "pop", changeHash: true });
        });
    },
    
getShoppingList:function()
    {
        var query = "SELECT * FROM tblShoppingCart";
        var productList = new Array();
        var aantalList = new Array();
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                          
                if(dataset.length == 0)
                {
                    $.mobile.changePage("#webshop-page", { transition: "flip", changeHash: true });
                          
                    navigator.notification.alert("U heeft nog geen producten toegevoegd aan de bestellijst.", function(){}, "Bestellijst", "OK");
                    
                }
                else
                {
                    for (var i = 0, item =null; i < dataset.length; i++)
                    {
                        item = dataset.item(i);
                        productList[i] = item['articleID'];
                        aantalList[i] = item['aantal'];
                    }
                    
                          $.ajax({
                                 url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                                 type: 'POST',
                                 data: { bestellijsttax:productList,aantal:aantalList},
                                 success: function(data) {
                                
                                    $('#bestellijst').empty();
                                    $('#bestellijst').append(data);
                                    $('#bestellijst').trigger('create');
                                 },
                                 error: function()
                                 {
                                    navigator.notification.alert("Het is niet gelukt om de bestelgegevens details te tonen.", function(){}, "Helaas", "OK");
                                 }
                                 });
                }
            });
        });
    },

checkTotalPrice:function()
    {
        var totalammount = document.getElementById('totalammount');
        if(totalammount.value < 5)
        {
            navigator.notification.alert("Het minimum bedrag voor bestellingen via de Apotheek App is 5 euro.", function(){}, "Orderbedrag te laag", "OK");
            dbase.checkForPharmacy('webshoppharmacy');
        }
        else
        {
            //$.mobile.changePage("#persoonsgegevens-page", { transition: "slide", changeHash: false });
            dbase.checkForPharmacy('webshoppharmacy');
        }
    },
    

    
removeProduct:function(productID)
    {
        var query = "DELETE FROM tblShoppingCart WHERE articleID ="+productID;
        
        db.transaction(function(tx)
                       {
                       tx.executeSql(query,[],function(tx, result)
                                     {
                                        webshop.getShoppingList();
                                     });
                       });

        
    },

updateAmount:function(articleID)
    {
        var newAmount = document.getElementById('aantal_'+articleID);
        var updateAmount = "UPDATE tblShoppingCart SET aantal = '"+newAmount.value+"' WHERE articleID = '"+articleID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(updateAmount);
            webshop.getShoppingList();
        });
    },

getPersonalData:function()
    {
        var query = "SELECT * FROM tblGegevens";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                                     
                if(dataset.length == 0)
                {
                    $.mobile.changePage("#persoonsgegevens-page", { transition: "pop", changeHash: true });
                }
                else
                {
                    $("#shop-voornaam").val(dataset.item(0)['firstname']);
                    $("#shop-tussenvoegsel").val(dataset.item(0)['prefix']);
                    $("#shop-achternaam").val(dataset.item(0)['lastname']);
                    $("#shop-geboortedatum").val(dataset.item(0)['birthdate']);
                    $("#shop-emailadres").val(dataset.item(0)['emailaddress']);
                    $("#shop-phonenumber").val(dataset.item(0)['phonenumber']);
                         
                    $.mobile.changePage("#persoonsgegevens-page", { transition: "pop", changeHash: true });
                }
            });
        });
    },
    
insertPersonalData:function(voornaam,tussenvoegsel,achternaam,geboortedatum,emailadres,telefoonnummer)
    {
        var deletequery = "DELETE FROM tblGegevens";
        var gegevensID = '1';
        var insertGegevens = "INSERT INTO tblGegevens (gegevensID, firstname, prefix, lastname, birthdate, emailaddress, phonenumber) VALUES (?,?,?,?,?,?,?)";
        
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(deletequery,[],dbase.succesCB,dbase.errorCB);
                       
            tx.executeSql(insertGegevens,[gegevensID, voornaam, tussenvoegsel, achternaam, geboortedatum, emailadres, telefoonnummer],dbase.succesCB,dbase.errorCB);
        });
    },
    
getIdealIssuers:function()
    {
        /**
         * check if form is filled in
         **/
        
        var shopvoornaam = document.getElementById('shop-voornaam');
        var shoptussenvoegsel = document.getElementById('shop-tussenvoegsel');
        var shopachternaam = document.getElementById('shop-achternaam');
        var shopgeboortedatum = document.getElementById('shop-geboortedatum');
        
        var shopemailadres = document.getElementById('shop-emailadres');
        var shopphonenumber = document.getElementById('shop-phonenumber');
        var goback = false;
        
        
        if(shopvoornaam.value == "" || shopachternaam.value == "" || shopgeboortedatum.value == "" || shopemailadres.value == "" || shopphonenumber.value == "" )
        {
            navigator.notification.alert("U heeft niet alle verplichte velden ingevuld.", function(){}, "Melding", "OK");
            var goback = true;
        }
        
        if(goback == false)
        {
            webshop.insertPersonalData(shopvoornaam.value,shoptussenvoegsel.value,shopachternaam.value,shopgeboortedatum.value,shopemailadres.value,shopphonenumber.value);
            
            $.mobile.changePage("#bankselection-page", { transition: "flip", changeHash: true });
            var type = "medsen";
            $('#banklist').empty();
            $.ajax({
                   url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                   type: 'POST',
                   data: { idealissuers:type},
                   success: function(data) {
                   
                   $('#banklist').append($('<label for="idealissuer" class="select">Selecteer uw bank</label><select name="idealissuer" id="idealissuer">'+data+'</select>'));
                   $('#banklist').trigger('create');
                   
                   
                   },
                   error: function()
                   {
                   navigator.notification.alert("Het is niet gelukt om de bestelgegevens details te tonen.", function(){}, "Helaas", "OK");
                   }
                   });
        }
    },
    
triggerPayment:function()
    {
        var query = "SELECT * FROM tblPharmacy";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Onvoldoende apotheek gegevens beschikbaar.", function(){}, "Foutmelding", "OK");
                }
                else
                {
                    
                    var apotheekNaam = dataset.item(0)['pharmacy'];

                    var issuer = document.getElementById('idealissuer');
                          
                    var totalammount = document.getElementById('totalammount');
                        
                    var random = Math.floor((Math.random() * 10000) + 1);
                    var transactionid = device.uuid+random;
                    var issuerid = issuer.value;
                    /* include pending ajax call to make sure, we are not going to miss any transactions if a customer does not click on the return or ok button */
                    webshop.pendingTransaction(transactionid,dataset.item(0)['agb']);
                    //alert(issuerid);
                         
                    $.ajax({
                        url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                        type: 'POST',
                           data: { transactionextended:issuerid,ammount:totalammount.value,transactionid:transactionid, apotheek:apotheekNaam},
                        success: function(data) {
                           
                        if(data != 'error')
                        {
                            var ref = window.open(data, '_blank', 'location=no');
                                 
                            ref.addEventListener('loadstart', function(event)
                            {
                                var urlSuccessPage = "https://pharmeon-socialmedia.nl/apotheekapp/store.php?transactionid="+transactionid;
                                
                                var urlCancelPage = "https://pharmeon-socialmedia.nl/apotheekapp/cancel.php?transactionid="+transactionid;
                                                 
                                if (event.url == urlSuccessPage) {
                                    ref.close();
                                    }
                                if (event.url == urlCancelPage) {
                                    ref.close();
                                }
                            });
                                 
                            ref.addEventListener('exit', function() {
                                var displayText = "Een moment geduld. Uw betaling wordt geverifieerd";
                                $('#idealNotification').empty();
                                $('#idealNotification').append(displayText);
                                $('#idealNotification').trigger('create');
                                        
                                webshop.confirmTransaction(transactionid,dataset.item(0)['agb']);
                                });
                           
                        }
                        else
                        {
                            navigator.notification.alert("Er is helaas iets misgegaan met het aanroepen van de iDEAL omgeving. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
                        }
                                 
                        },
                        error: function()
                            {
                                navigator.notification.alert("Het is niet gelukt om de bestelgegevens details te tonen.", function(){}, "Helaas", "OK");
                            }
                    });
                }
            });
        });
        
        
    },

pendingTransaction:function(transactionid,agb)
    {
        var pendingshopvoornaam = document.getElementById('shop-voornaam');
        var pendingshoptussenvoegsel = document.getElementById('shop-tussenvoegsel');
        var pendingshopachternaam = document.getElementById('shop-achternaam');
        var pendingshopgeboortedatum = document.getElementById('shop-geboortedatum');
        var pendingshopemailadres = document.getElementById('shop-emailadres');
        var pendingshopphonenumber = document.getElementById('shop-phonenumber');
        
        var query = "SELECT * FROM tblShoppingCart";
        var pendingproductList = new Array();
        var pendingaantalList = new Array();
        
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                          
                }
                else
                {
                    for (var i = 0, item =null; i < dataset.length; i++)
                    {
                        item = dataset.item(i);
                        pendingproductList[i] = item['articleID'];
                        pendingaantalList[i] = item['aantal'];
                    }
                          
                    $.ajax({
                        url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                        type: 'POST',
                        data: { pendingorder:transactionid,agb:agb,voornaam:pendingshopvoornaam.value,tussenvoegsel:pendingshoptussenvoegsel.value, achternaam:pendingshopachternaam.value,geboortedatum:pendingshopgeboortedatum.value,emailadres:pendingshopemailadres.value,phonenumber:pendingshopphonenumber.value,bestelling:pendingproductList,aantal:pendingaantalList},
                        success: function(data) {
                           
                        },
                        error: function()
                        {
                            navigator.notification.alert("Uw betaling hebben we niet kunnen controleren.", function(){}, "Helaas", "OK");
                        }
                    });
                }
            });
        });
    },

confirmTransaction:function(transactionid,agb)
    {
        var shopvoornaam = document.getElementById('shop-voornaam');
        var shoptussenvoegsel = document.getElementById('shop-tussenvoegsel');
        var shopachternaam = document.getElementById('shop-achternaam');
        var shopgeboortedatum = document.getElementById('shop-geboortedatum');
        var shopemailadres = document.getElementById('shop-emailadres');
        var shopphonenumber = document.getElementById('shop-phonenumber');
        
        var query = "SELECT * FROM tblShoppingCart";
        var productList2 = new Array();
        var aantalList2 = new Array();
        
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                        
                }
                else
                {
                    for (var i = 0, item =null; i < dataset.length; i++)
                    {
                        item = dataset.item(i);
                        productList2[i] = item['articleID'];
                        aantalList2[i] = item['aantal'];
                    }
                    
                    $.ajax({
                           url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                           type: 'POST',
                           data: { statuscheckupdate:transactionid,agb:agb,voornaam:shopvoornaam.value,tussenvoegsel:shoptussenvoegsel.value, achternaam:shopachternaam.value,geboortedatum:shopgeboortedatum.value,emailadres:shopemailadres.value,phonenumber:shopphonenumber.value,bestelling:productList2,aantal:aantalList2},
                           success: function(data) {
                           
                            if(data == "completed")
                            {
                                var displayText = "Uw bestelling is betaald en wordt verzonden naar de apotheek.";
                                $('#idealNotification').empty();
                                $.mobile.changePage("#dashboard-page", { transition: "flip", changeHash: true });
                                navigator.notification.alert("Uw bestelling is betaald en wordt verzonden naar de apotheek.", function(){}, "Status", "OK");
                                 
                            }
                            else
                            {
                                var displayText = "De betaling van uw bestelling is mislukt.";
                                $('#idealNotification').empty();
                                $.mobile.changePage("#dashboard-page", { transition: "flip", changeHash: true });
                                navigator.notification.alert("Uw betaling wordt geverifieerd en na akkoord verzonden naar de apotheek. ", function(){}, "Status", "OK");
                            }
                           },
                            error: function()
                            {
                                 navigator.notification.alert("Uw betaling hebben we niet kunnen controleren.", function(){}, "Helaas", "OK");
                            }
                        });
                
                
                          
                }
            });
        });
    },
};

// Database
var db = window.openDatabase("apotheekapp", "1.0", "Apotheek App", 200000);

var dbase = {
encrypt:function(value)
    {
        var deviceID = device.uuid;
        var encrypted = CryptoJS.AES.encrypt(value, deviceID);
        
        return encrypted;
    },
    
decrypt:function(value)
    {
        var deviceID = device.uuid;
        var decrypted = CryptoJS.AES.decrypt(value, deviceID);
        
        return decrypted.toString(CryptoJS.enc.Utf8);
    },
    
openDB:function()
    {
        db.transaction(dbase.createDB, dbase.errorCB, dbase.succesCB);
    },
 
dropTable:function()
    {
        var query = "DELETE FROM tblMedication";
        
        db.transaction(function(tx)
                       {
                       tx.executeSql(query,[],function(tx, result)
                                     {
                                     
                                     });
                       });
    },
createDB:function(tx)
    {
        //tx.executeSql('DROP TABLE IF EXISTS tblaccount'); //please remove this line before golive!
        //tx.executeSql('CREATE TABLE IF NOT EXISTS tblaccount (accountID INTEGER, pharmacy TEXT, mobilenumber TEXT, accountName TEXT)');
        
        tx.executeSql('DROP TABLE IF EXISTS tbldeviceToken'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tbldeviceToken (tokenID INTEGER, deviceToken TEXT)');
        
        //tx.executeSql('DROP TABLE IF EXISTS tblPharmacy'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblPharmacy (agb INTEGER, pharmacy TEXT)');
        
        //tx.executeSql('DROP TABLE IF EXISTS tblhhrApi'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblhhrApi (userID INTEGER, apiKey TEXT)');
        
        //tx.executeSql('DROP TABLE IF EXISTS tblShoppingCart'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblShoppingCart (articleID TEXT, aantal TEXT)');
        
        //tx.executeSql('DROP TABLE IF EXISTS tblMedication'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblMedication (medicationID TEXT, medicationName TEXT, status TEXT)');
               
        // tx.executeSql('DROP TABLE IF EXISTS tblTermsAndConditions'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblTermsAndConditions (seen BOOL)');

        tx.executeSql('CREATE TABLE IF NOT EXISTS tblGegevens (gegevensID TEXT, firstname TEXT, prefix TEXT,lastname TEXT,birthdate TEXT,emailaddress TEXT,phonenumber TEXT)');
       
        //tx.executeSql('DROP TABLE IF EXISTS tblMedication2'); //please remove this line before golive!
        //tx.executeSql('CREATE TABLE IF NOT EXISTS tblMedication2 (medicationID TEXT, medicationName TEXT, status TEXT)');
        
        //tx.executeSql('DROP TABLE IF EXISTS tblHistory'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblHistory (medicationID TEXT, orderDate TEXT)');
        
        //tx.executeSql('DROP TABLE IF EXISTS tblRemovedMedication'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblRemovedMedication (removeID TEXT, medicationID TEXT, adviesvan TEXT, reden TEXT, toelichting TEXT)');
        
        //tx.executeSql('DROP TABLE IF EXISTS tblUpdatedPrescription'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblUpdatedPrescription (updatedID TEXT, medicationID TEXT, adviesvan TEXT, reden TEXT, toelichting TEXT)');
        
        //tx.executeSql('DROP TABLE IF EXISTS tblIntakeHistory'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblIntakeHistory (intakeID NUMERIC, scheduleID TEXT, medicationID TEXT, intakeDate TEXT, intakeTime TEXT, taken TEXT, medicationArchiveName TEXT)');
        
        //tx.executeSql('DROP TABLE IF EXISTS tblScheduledIntakes'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblScheduledIntakes (scheduleID TEXT, medicationID TEXT, interval TEXT, intakeDateTime TEXT, status TEXT, medicationArchiveName TEXT)');
        
        //tx.executeSql('DROP TABLE IF EXISTS tblExperiences'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblExperiences (experienceID TEXT, medicationID TEXT, effectiviteit TEXT, bijwerkingen TEXT, gebruiksgemak TEXT, algehelewaardering TEXT, opmerkingen TEXT, experienceDate TEXT)');
        
        //tx.executeSql('DROP TABLE IF EXISTS tblAddedMedication'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblAddedMedication (addedMedicationID TEXT, addedMedication TEXT, addedSterkte TEXT, addedAantal TEXT, addedGebruik TEXT, addedVoorschrijver TEXT, addedStartdatum TEXT, addedEinddatum TEXT, addedOpmerkingen TEXT, status TEXT)');
        
        //tx.executeSql('DROP TABLE IF EXISTS tblPincode'); //please remove this line before golive!
        tx.executeSql('CREATE TABLE IF NOT EXISTS tblPincode (pincode TEXT, enterdate TEXT)');
        
        dbase.alterTable('tblMedication');
    },

 /**
  * new function
  **/

    //medicationID TEXT, medicationName TEXT, status TEXT
    //tx.executeSql('CREATE TABLE IF NOT EXISTS tblMedicationAIS (medicationID TEXT, medicationName TEXT, status TEXT, startDate TEXT, endDate TEXT, prescriberName TEXT, usageText TEXT)');

alterTable:function(tablename)
    {
        var extendStartDate = "ALTER TABLE " + tablename +" ADD COLUMN startDate TEXT";
        var extendEndDate = "ALTER TABLE " + tablename +" ADD COLUMN endDate TEXT";
        var extendPrescriberName = "ALTER TABLE " + tablename +" ADD COLUMN prescriberName TEXT";
        var extendUsageText = "ALTER TABLE " + tablename +" ADD COLUMN usageText TEXT";
        //var addRegistrationType = "ALTER TABLE " + tablename +" ADD COLUMN registration TEXT";
        var addIdentifier = "ALTER TABLE tblPharmacy ADD COLUMN identifier TEXT";
        var addProactiefOption = "ALTER TABLE tblMedication ADD COLUMN proactiefOption TEXT";
        var addHistoryName = "ALTER TABLE tblHistory ADD COLUMN historyName TEXT";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(extendStartDate,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(extendEndDate,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(extendPrescriberName,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(extendUsageText,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(addIdentifier,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(addProactiefOption,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(addHistoryName,[],dbase.succesCB,dbase.errorCB);
        });
    },

    
checkForPharmacy:function(redirect)
    {
        var query = "SELECT * FROM tblPharmacy";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                
                if(dataset.length == 0)
                {
                    /**
                    * in de nieuwe situatie direct doorgaan naar het dashboard, niet meer verplicht een apotheek kiezen.
                    **/
                    if(redirect == 'dashboard')
                    {
                        //$.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: false });
                    }
                    if(redirect == 'pharmacyselect')
                    {
                        setup.pharmacySelect(redirect);
                    }
                    if(redirect == 'webshoppharmacy')
                    {
                        setup.pharmacySelect(redirect);
                    }
                    if(redirect == 'pharmacyinfo')
                    {
                          setup.pharmacySelect(redirect);
                    }
                    
                }
                else
                {
                    if(redirect == 'webshoppharmacy')
                          {
                          //$.mobile.changePage("#persoonsgegevens-page", { transition: "pop", changeHash: false });
                          webshop.getPersonalData();
                          }
                    else
                    {
                        $("#appointmentagb").val(dataset.item(0)["agb"]);
                        dbase.checkForAccount(redirect);
                    }
                }
            });
        });
    },
    
checkForAccount:function(redirect)
    {
        var query = "SELECT * FROM tblhhrApi";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                          
                if(dataset.length == 0)
                {
                    if(redirect == "dashboard")
                    {
                        $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
                    }
                    else
                    {
                        $.mobile.changePage("#login-page", { transition: "pop", changeHash: true });
                    }
                }
                else
                {
                    $("#loadMed").show();

                    app.prepareMedicationData();
                    app.getPharmacyName();
                }
            });
        });
    },
 
insertKey:function(apiKey)
    {
        var insertKey = "INSERT INTO tblhhrApi (userID, apiKey) VALUES (?,?)";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(insertKey,[1, apiKey],dbase.succesCB,dbase.errorCB);
        });
        
        $("#loading").hide();
        $("#loadingverify").hide();
        
        app.prepareMedicationData();
        
        $.mobile.changePage("#dashboard-page", { transition: "flip", changeHash: true });
    },

insertMedication:function(MedicationId, MedicationName, startDate, endDate,prescriberName,usageText)
    {
        var status = "no";
        
        //var insertMedication = "INSERT INTO tblMedication (medicationID, medicationName,status) VALUES (?,?,?)";
        var insertMedication = "INSERT INTO tblMedication (medicationID, medicationName, status, startDate, endDate, prescriberName, usageText) VALUES (?,?,?,?,?,?,?)";

        
        db.transaction(function(tx, results)
        {
            //tx.executeSql(insertMedication,[MedicationId, MedicationName,status],dbase.succesCB,dbase.errorCB);
            tx.executeSql(insertMedication,[MedicationId, MedicationName, status, startDate, endDate, prescriberName, usageText],dbase.succesCB,dbase.errorCB);
        });
    },
    
errorCB:function(tx, err)
    {
        console.log("error processing SQL: "+err.code);
        //alert("error message::"+err.message);
    },
 
errorCB2:function(tx, err)
    {
        //console.log("error processing SQL: "+err.code);
        //alert("error message::"+err.code);
    },
succesCB2:function()
    {
        //alert("Query is correct uitgevoerd");
    },
succesCB:function()
    {
        //alert("Query is correct uitgevoerd");
    }
    
};

var intakes = {
    intakeSchedule:function(medicationID)
    {
        app.showMedicationName(medicationID,'intakeName');
        app.showUsageText(medicationID,'intakeUsageText');
        $.mobile.changePage("#intakeschedule-page", { transition: "pop", changeHash: true });
        
        var query = "SELECT * FROM tblScheduledIntakes WHERE medicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                                     
                if(dataset.length == 0)
                {
                    $('#intakeHR').show();
                    $('#setIntakes').empty();
                    $('#setIntakes').append('<small><center>U heeft nog geen Inname schema voor dit medicijn ingesteld. Klik op de button hieronder om een inname moment in te stellen. </center></small>');
                }
                else
                {
                    $('#intakeHR').hide();
                    $('#setIntakes').empty();
                    $('#setIntakes').append('<div class="intakeList"><strong>Ingesteld inname schema</strong></div>');
                                     
                    for (var i = 0, item =null; i < dataset.length; i++)
                    {
                        item = dataset.item(i);
                        var medinterval = "Wekelijks";
                        
                        if(item['interval'] == 'daily')
                        {
                            var medinterval = "Dagelijks";
                        }
                        if(item['interval'] == 'skipday')
                        {
                            var medinterval = "Om de dag";
                        }
                        if(item['interval'] == 'monthly')
                        {
                            var medinterval = "Maandelijks";
                        }
                        
                        $('#setIntakes').append('<div class="intakeList"><div style="float: left;">'+medinterval+ ' om '+ item['intakeDateTime']+'</div><div class="intakeDeleteOption" style="float: right;"><small style="text-align:right;"><a href="#" onclick="intakes.deleteIntakeReminder(\''+item['scheduleID']+'\',\''+medicationID+'\');"><i class="fa fa-times" style="padding-right: 5px;"></i>verwijderen</a></small></div><div class="clearer"></div></div>');
                    }
                }
            });
        });
        
        $('#medActionButtons').empty();
        $('#medActionButtons').append('<a href="#" class="ui-btn ui-corner-all addSchemaButton" onclick="intakes.addIntakeSchedule(\''+medicationID+'\');" data-transition="pop"><i class="fa fa-plus fa-lg"></i> Inname herinnering toevoegen</a>');
    },
    
    addIntakeSchedule:function(medicationID)
    {
        
        $("#schedulemedid").val(medicationID);
        
        $.mobile.changePage("#addintakeschedule-page", { transition: "pop", changeHash: true });
        
        app.insertMedicationName(medicationID,'medNameField');
        app.showMedicationName(medicationID,'addIntakeName');
        app.showUsageText(medicationID,'addIntakeUsageText');
    },
    
    saveIntakeReminder:function()
    {
        if(interval.value == "" || tijdstip.value == "" )
        {
            navigator.notification.alert("U heeft het tijdstip voor uw inname niet ingevoerd.", function(){}, "Melding", "OK");
            window.scrollTo(0,0);
            return;
        }
        
        var scheduleID = (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        var intakeID = Math.floor(100000 + Math.random() * 900000);
        
        var gettime = tijdstip.value;
        var time = gettime.split(":");
        
        var d = new Date();
        
        var calculateAlarm = new Date();
        calculateAlarm.setHours(time[0], time[1], 1);
        
        if(calculateAlarm.getTime() <= d.getTime())
        {
            var day = d.getDate()+1;
            var month = d.getMonth()+1;
            var year = d.getFullYear();
        }
        else
        {
            var day = d.getDate();
            var month = d.getMonth()+1;
            var year = d.getFullYear();
        }
        
        var currentdate = day+'-'+month+'-'+year;
        
        var insertIntakeReminder = "INSERT INTO tblScheduledIntakes (scheduleID, medicationID, interval, intakeDateTime, medicationArchiveName) VALUES (?,?,?,?,?)";
        
        var insertIntakeHistory = "INSERT INTO tblIntakeHistory (intakeID, scheduleID, medicationID, intakeDate, intakeTime, taken, medicationArchiveName) VALUES (?,?,?,?,?,?,?)";
        
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(insertIntakeReminder,[scheduleID, schedulemedid.value, interval.value, tijdstip.value, medNameField.value],dbase.succesCB,dbase.errorCB);
                       
            tx.executeSql(insertIntakeHistory,[intakeID, scheduleID, schedulemedid.value, currentdate,tijdstip.value,'pending', medNameField.value],dbase.succesCB,dbase.errorCB);
                       
            notifications.createIntakeReminder(schedulemedid.value, scheduleID, intakeID);
            intakes.intakeSchedule(schedulemedid.value);
            //stats.intakeReminders(schedulemedid.value,'added');
        });
    },
    
    deleteIntakeReminder:function(intakeID,medicationID)
    {
        var deletequery = "DELETE FROM tblScheduledIntakes WHERE scheduleID = '"+intakeID+"'";
        var query = "SELECT * FROM tblIntakeHistory WHERE scheduleID = '"+intakeID+"' AND taken='pending'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(deletequery,[],function(tx, result)
            {
                intakes.intakeSchedule(medicationID);
            });
                       
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                                     
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Er is iets misgegaan bij het verwijderen van het inname schema.", function(){}, "Foutmelding", "OK");
                }
                else
                {
                    intakes.removeNotification(dataset.item(0)["intakeID"]);
                    //stats.intakeReminders(medicationID,'removed');
                }
            });
        });
    },
    
    removeNotification:function(intakeID)
    {
        var deleteNotification = "DELETE FROM tblIntakeHistory WHERE intakeID = '"+intakeID+"'";
        db.transaction(function(tx)
        {
            tx.executeSql(deleteNotification,[],function(tx, result)
            {
                window.plugin.notification.local.cancel(intakeID);
            });
        });
    },

    checkForIntakes:function()
    {
        var query = "SELECT * FROM tblIntakeHistory";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                          
                if(dataset.length == 0)
                {
                    $('#intakeHistoryBox').hide();
                }
                else
                {
                    $('#intakeHistoryBox').show();
                }
            });
        });
    },
    

    getMedicationName:function(triggerdID)
    {
        var query = "SELECT * FROM tblIntakeHistory LEFT JOIN tblMedication ON tblMedication.medicationID = tblIntakeHistory.medicationID LEFT JOIN tblAddedMedication ON tblAddedMedication.addedMedicationID = tblIntakeHistory.medicationID WHERE tblIntakeHistory.intakeID = '"+triggerdID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    //query on the local database
                    navigator.notification.alert("Geen inname moment gevonden.", function(){}, "Foutmelding", "OK");
                }
                else
                {
                    var scheduleID = dataset.item(0)["scheduleID"];
                    if(dataset.item(0)["medicationArchiveName"] == null)
                    {
                        var medicationArchiveName = dataset.item(0)["medicationName"];
                    }
                    else
                    {
                        var medicationArchiveName = dataset.item(0)["medicationArchiveName"];
                    }
                          
                    navigator.notification.confirm('Heeft u het geneesmiddel '+medicationArchiveName+ ' ingenomen',
                                                                    
                    function(button)
                    {
                        if (button == 1)
                        {
                            intakes.registerIntake(triggerdID,'yes',scheduleID);
                        }
                        else
                        {
                            intakes.snoozeQuestion(triggerdID,'no',scheduleID,medicationArchiveName);
                        }
                    },
                    'Ingenomen?',
                    ['Ja','Nee']
                    );
                }
            },dbase.succesCB,dbase.errorCB);
        });
    },
    
    snoozeQuestion:function(triggerID, taken, scheduleID, medicationName)
    {
        navigator.notification.confirm('Wilt u over 60 minuten nogmaals een herinnering ontvangen?',
        function(button)
        {
            if (button == 1)
            {
                var now                  = new Date().getTime(),
                _60_seconds_from_now = new Date(now + 60*1000);
                snooze = new Date(now + 3600*1000);
                                       
                //alert('actual alarm for test purposes: '+snooze);
                
                cordova.plugins.notification.local.schedule({
                    id: triggerID,
                    text: 'Herinnering voor het inname moment van '+medicationName+' ',
                    badge: 1,
                    date: snooze
                });
                
                intakes.updateTimeForSnooze(triggerID);
            }
            else
            {
                intakes.registerIntake(triggerID,'no',scheduleID);
            }
        },
        'Snooze?',
        ['Ja','Nee']
        );

    },
    
    updateTimeForSnooze:function(triggerID)
    {
        var now = new Date().getTime();
        
        var d = new Date(now+ 3600*1000),
        h = (d.getHours()<10?'0':'') + d.getHours(),
        m = (d.getMinutes()<10?'0':'') + d.getMinutes();
        var snoozetime = h+':'+m;
        
        var updateIntakeTime = "UPDATE tblIntakeHistory SET intakeTime = '"+snoozetime+"' WHERE intakeID = '"+triggerID+"'";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(updateIntakeTime,dbase.succesCB2,dbase.errorCB2);
        });
    },
    
    registerIntake:function(triggerID, taken, scheduleID)
    {
        var updateMedStatus = "UPDATE tblIntakeHistory SET taken = '"+taken+"' WHERE intakeID = '"+triggerID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(updateMedStatus);
            intakes.getIntervalforReminder(triggerID,scheduleID);
        });
    },
    
    getIntervalforReminder:function(intakeID, scheduleID)
    {
        var query = "SELECT interval FROM tblScheduledIntakes WHERE tblScheduledIntakes.scheduleID = '"+scheduleID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                                     
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Er is een onverwachte fout opgetreden.", function(){}, "Foutmelding", "OK");
                }
                else
                {
                    intakes.renewIntakeReminder(intakeID,dataset.item(0)["interval"]);
                }
            });
        });
    },
    
    renewIntakeReminder:function(intakeID, interval)
    {
        var query = "SELECT * FROM tblIntakeHistory LEFT JOIN tblScheduledIntakes ON tblScheduledIntakes.scheduleID = tblIntakeHistory.scheduleID LEFT JOIN tblMedication ON tblMedication.medicationID = tblIntakeHistory.medicationID LEFT JOIN tblAddedMedication ON tblAddedMedication.addedMedicationID = tblIntakeHistory.medicationID WHERE tblIntakeHistory.intakeID = '"+intakeID+"' ORDER BY tblIntakeHistory.intakeID DESC";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                                     
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Er is een onverwachte fout opgetreden.", function(){}, "Foutmelding", "OK");
                }
                else
                {
                    //function toevoegen voor het ophalen van de interval
                    var currentName = dataset.item(0)["medicationName"];
                    if(dataset.item(0)["medicationName"] == null)
                    {
                        var currentName = dataset.item(0)["addedMedication"]
                    }
                          
                    var scheduleID = dataset.item(0)["scheduleID"];
                          
                    if(dataset.item(0)["medicationArchiveName"] == null)
                    {
                        var medicationName = currentName;
                        //alert('fallback is set');
                    }
                    else
                    {
                        var medicationName = dataset.item(0)["medicationArchiveName"];
                    }
                        //var medicationName = dataset.item(0)["medicationArchiveName"];
                                     
                    var medicationID = dataset.item(0)["medicationID"];
                    if(dataset.item(0)["medicationID"] == null)
                    {
                        var medicationID = dataset.item(0)["addedMedicationID"]
                    }
                                     
                    var intakeDate = dataset.item(0)["intakeDate"];
                    var tijdstip = dataset.item(0)["intakeDateTime"];
                
                    if(currentName == '')
                    {
                        notifications.trigger();
                    }
                    else
                    {
                        intakes.setNewIntakeReminder(scheduleID,medicationID,intakeDate,tijdstip,medicationName, interval);
                    }
                }
            });
        });
    },
    
setNewIntakeReminder:function(scheduleID, medicationID, intakeDate, intakeTime, medicationName, interval)
    {
        var newIntakeID = Math.floor(100000 + Math.random() * 900000);
        
        var insertIntakeHistory = "INSERT INTO tblIntakeHistory (intakeID, scheduleID, medicationID, intakeDate,intakeTime, taken, medicationArchiveName) VALUES (?,?,?,?,?,?,?)";
        
        db.transaction(function(tx, results)
        {
            //based on interval next intake date calculeren
            var gettime = intakeTime;
            var time = gettime.split(":");
                       
            var intakeAlarm = new Date();
            intakeAlarm.setHours(time[0], time[1], 1);
            if(interval == 'daily')
            {
                var nextDay = new Date(intakeAlarm);
                nextDay.setHours(nextDay.getHours() + 24);
                    
                var nextDag = nextDay.getDate();
                var nextMaand = nextDay.getMonth()+1;
                var nextJaar = nextDay.getFullYear();
                var tomorrow = nextDag+'-'+nextMaand+'-'+nextJaar;
            }
            if(interval == 'skipday')
            {
                var nextDay = new Date(intakeAlarm);
                nextDay.setHours(nextDay.getHours() + 48);
                       
                var nextDag = nextDay.getDate();
                var nextMaand = nextDay.getMonth()+1;
                var nextJaar = nextDay.getFullYear();
                var tomorrow = nextDag+'-'+nextMaand+'-'+nextJaar;
            }
            if(interval == 'weekly')
            {
                var nextDay = new Date(intakeAlarm);
                nextDay.setHours(nextDay.getHours() + 168);
                       
                var nextDag = nextDay.getDate();
                var nextMaand = nextDay.getMonth()+1;
                var nextJaar = nextDay.getFullYear();
                var tomorrow = nextDag+'-'+nextMaand+'-'+nextJaar;
            }
            if(interval == 'skipweek')
            {
                var nextDay = new Date(intakeAlarm);
                nextDay.setHours(nextDay.getHours() + 336);
                       
                var nextDag = nextDay.getDate();
                var nextMaand = nextDay.getMonth()+1;
                var nextJaar = nextDay.getFullYear();
                var tomorrow = nextDag+'-'+nextMaand+'-'+nextJaar;
            }
            if(interval == 'monthly')
            {
                var nextDay = new Date(intakeAlarm);
                nextDay.setMonth(nextDay.getMonth() +1);
                       
                var nextDag = nextDay.getDate();
                var nextMaand = nextDay.getMonth() +1;
                var nextJaar = nextDay.getFullYear();
                var tomorrow = nextDag+'-'+nextMaand+'-'+nextJaar;
            }
                //alert(tomorrow);
                       
            tx.executeSql(insertIntakeHistory,[newIntakeID, scheduleID, medicationID, tomorrow, intakeTime,'pending', medicationName],dbase.succesCB,dbase.errorCB2);
                       
            //var now                  = new Date().getTime(),
            //_60_seconds_from_now = new Date(now + 20*1000);
            //alert('actual alarm for test purposes: '+nextDay);
            
            cordova.plugins.notification.local.schedule({
                id: newIntakeID,
                text: 'inname moment voor '+medicationName+' ',
                badge: 1,
                date: nextDay
            });
        });
    },
    //INTAKE HISTORY FUNCTIONS
    getIntakeHistory:function()
    {
        
        $.mobile.changePage("#intakehistory-page", { transition: "slide", changeHash: true });
        
        var query = "SELECT * FROM tblIntakeHistory GROUP BY intakeDate ORDER BY strftime('%s', intakeDate) DESC";
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    $('#intakeHistory').empty();
                    $('#intakeHistory').append("<div class='promoText'><p><strong>U heeft nog geen inname herinneringen ingesteld.</strong></p><p>In het medicatieoverzicht kunt u per medicijn één of meerdere inname herinneringen instellen.</p></div>");
                }
                else
                {
                    var sortingdate = new Array();
                                     
                    $('#intakeHistory').empty();
                    for (var i = 0, item =null; i < dataset.length; i++)
                    {
                        item = dataset.item(i);
                          
                        var sortdate = item['intakeDate'].split("-");
                        var sortday = sortdate[0];
                        if(sortday < 10)
                        {
                            sortday = '0'+sortday;
                        }
                        var sortmonth = sortdate[1];
                        var sortyear = sortdate[2];
                                     
                        var reformattedDate = ''+sortyear+'-'+sortmonth+'-'+sortday;
                                     
                        sortingdate[sortingdate.length] = reformattedDate;
                    }
                    sortingdate.sort();
                    sortingdate.reverse(); //somehow this function became obsolete
                    
                    intakes.createIntakeHistoryOrdering(sortingdate);
                                     
                }
            });
        });
    },
    
createIntakeHistoryOrdering:function(sortingdate)
    {
        for (var i = 0, item =null; i < sortingdate.length; i++)
        {
            //alert(sortingdate[i]);
            var a = sortingdate[i].split("-");
            var aday = a[2];
            
            if(aday < 10)
            {
                aday = aday.substr(1);
            }
            var amonth = a[1];
            var ayear = a[0];
            var intakeDate = aday+'-'+amonth+'-'+ayear;
            
            switch (amonth) {
                case "1":
                    var amonth = 'januari';
                    break;
                case "2":
                    var amonth = 'februari';
                    break;
                case "3":
                    var amonth = 'maart';
                    break;
                case "4":
                    var amonth = 'april';
                    break;
                case "5":
                    var amonth = 'mei';
                    break;
                case "6":
                    var amonth = 'juni';
                    break;
                case "7":
                    var amonth = 'juli';
                    break;
                case "8":
                    var amonth = 'augustus';
                    break;
                case "9":
                    var amonth = 'september';
                    break;
                case "10":
                    var amonth = 'oktober';
                    break;
                case "11":
                    var amonth = 'november';
                    break;
                case "12":
                    var amonth = 'december';
                    break;
            }
            
            var formatedIntakeDate = aday+' '+amonth+' '+ayear;
            //alert(intakeDate);
            //alert(formatedIntakeDate)
            intakes.createIntakeHistoryList(intakeDate,formatedIntakeDate);
        }
        
    },
    
createIntakeHistoryList:function(intakeDate, formatedIntakeDate)
    {
        var query = "SELECT * FROM tblIntakeHistory LEFT JOIN tblMedication ON tblMedication.medicationID = tblIntakeHistory.medicationID LEFT JOIN tblAddedMedication ON tblAddedMedication.addedMedicationID = tblIntakeHistory.medicationID WHERE tblIntakeHistory.intakeDate = '"+intakeDate+"' ORDER BY tblIntakeHistory.intakeTime";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    $('#intakeHistory').append('Geen innamen voor deze datum gevonden');
                }
                else
                {
                    $('#intakeHistory').append('<div class="dateHeader">'+formatedIntakeDate+'</div>');
                    for (var i = 0, item =null; i < dataset.length; i++)
                    {
                        item = dataset.item(i);
                                     
                        switch (item['taken'])
                        {
                            case "yes":
                                var taken = '<i class="fa fa-check"></i>';
                            break;
                            case "no":
                                var taken = '<i class="fa fa-times"></i>';
                            break;
                            case "pending":
                                var taken = '<i class="fa fa-calendar"></i>';
                            break;
                        }
                                     
                        var medicationName = item['medicationName'];
                        if(item['medicationName'] == null)
                        {
                            var medicationName = item['addedMedication'];
                        }
                            //alert(medicationName);
                        
                        if(item['medicationArchiveName'] == null)
                        {
                            var medicatieNaam = medicationName;
                        }
                        else
                        {
                            var medicatieNaam = item['medicationArchiveName'];
                        }
                                     //alert(medicatieNaam);
                        $('#intakeHistory').append('<div class="dateContainer"><div class="dateData" style="width:9% !important;">'+item['intakeTime']+'</div><div class="dateData" style="width:70% !important;" onclick="app.deleteIntakeConfirmation(\''+item['intakeID']+'\');">'+medicatieNaam+'</div><div class="dateData floatright">'+taken+'</div><div class="clearer"></div></div>');
                    }
                }
            });
        });
    },
    
deleteIntakeConfirmation:function(id)
    {
        navigator.notification.confirm(
                                       'Wilt u deze inname verwijderen uit de lijst?',
                                       function(button)
                                       {
                                       if (button == 1)
                                       {
                                       app.deleteIntakeHistoryItem(id);
                                       }
                                       },
                                       'Bevestigen',
                                       ['Ja','Nee']
                                       );
        
    },
    
deleteIntakeHistoryItem:function(id)
    {
        var deleteIntake = "DELETE FROM tblIntakeHistory WHERE intakeID = '"+id+"'";
        
        db.transaction(function(tx)
                       {
                       tx.executeSql(deleteIntake,[],function(tx, result)
                                     {
                                     app.getIntakeHistory();
                                     });
                       });
    },

};

var notifications = {
    
createIntakeReminder:function(medicationID, scheduleID, intakeID)
    {
        var gettime = tijdstip.value;
        var time = gettime.split(":");
        
        var d = new Date();
        
        var calculateAlarm = new Date();
        calculateAlarm.setHours(time[0], time[1], 1);
        
        
        if(calculateAlarm.getTime() <= d.getTime())
        {
            var intakeAlarm = new Date(calculateAlarm);
            intakeAlarm.setHours(intakeAlarm.getHours()+24);
        }
        else
        {
            var intakeAlarm = new Date(calculateAlarm);
        }
        
        var now             = new Date().getTime(),
        _15_sec_from_now = new Date(now + 15*1000);
        
        cordova.plugins.notification.local.schedule({
            id: intakeID,
            text: 'inname moment voor '+medNameField.value+' ',
            badge: 1,
            date: intakeAlarm
        });
        
        intakes.checkForIntakes();
    },
    
set:function()
    {
        var medicationName = 'test medicijn';
        var now             = new Date().getTime(),
        _15_sec_from_now = new Date(now + 15*1000);
        
        cordova.plugins.notification.local.schedule({
                                                    id: 123,
                                                    text: 'inname moment voor '+medicationName+' ',
                                                    badge: 1,
                                                    date: _15_sec_from_now
                                                    });
    },
trigger:function()
    {
        /*
        //TRIGGER EVENT, WHEN CLICKED FROM NOTIFICATION CENTER
        cordova.plugins.notification.local.on("click", function (notification)
        {
            intakes.getMedicationName(notification.id);
            cordova.plugins.notification.local.clearAll(function() {}, this);

        });
        */
        
        //TRIGGER EVENT, TRIGGERS WHEN APP IS OPEN
        cordova.plugins.notification.local.on("trigger", function(notification)
        {
            intakes.getMedicationName(notification.id);
            cordova.plugins.notification.local.clearAll(function() {}, this);
        });
        /*
        //TRIGGER EVENT, WHEN APP IS CLOSED AND THEN OPENED
        cordova.plugins.notification.local.getTriggered(function (notifications)
        {
            if(notifications.length != 0 )
            {
                for (var i = 0; i < notifications.length; i++)
                {
                    intakes.getMedicationName(notifications[i].id);
                    var counter = i+1;
                    if(counter == notifications.length)
                    {
                        cordova.plugins.notification.local.clearAll(function() {}, this);
                    }
                }
            }
        });
         */
        var query = "SELECT * FROM tblScheduledIntakes LEFT JOIN tblIntakeHistory ON tblIntakeHistory.scheduleID = tblScheduledIntakes.scheduleID AND tblIntakeHistory.taken = 'pending'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                                     
                if(dataset.length == 0)
                {
                    //alert('geen innamen');
                }
                else
                {
                    for (var i = 0, item =null; i < dataset.length; i++)
                    {
                        item = dataset.item(i);
                                     
                        var intakeDate = item['intakeDate'];
                        var intakeTime = item['intakeTime'];
                          
                        var gettime = intakeTime;
                        var time = gettime.split(":");
                        var reminderdate = intakeDate.split("-");
                        var intakeAlarm = new Date(reminderdate[2],reminderdate[1]-1,reminderdate[0]);
                                     
                        intakeAlarm.setHours(time[0], time[1], 1);
                        var reminderDayTime = new Date(intakeAlarm);
                        var currentday = new Date();
                        if (reminderDayTime <= currentday)
                        {
                          //alert(intakeTime);
                            intakes.getMedicationName(item['intakeID']);
                          
                            cordova.plugins.notification.local.clearAll(function() {
                                //alert("verwijderd");
                            }, this);
                        }
                        else
                        {
                            //alert('reminder moet nog afgaan');
                        }
                    }
                }
            });
        });
    },

checkfortriggers:function()
    {
        var query = "SELECT * FROM tblScheduledIntakes LEFT JOIN tblIntakeHistory ON tblIntakeHistory.scheduleID = tblScheduledIntakes.scheduleID AND tblIntakeHistory.taken = 'pending'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                                     
                if(dataset.length == 0)
                {
                    //alert('geen innamen');
                }
                else
                {
                    for (var i = 0, item =null; i < dataset.length; i++)
                    {
                        item = dataset.item(i);
                          
                        var intakeDate = item['intakeDate'];
                        var intakeTime = item['intakeTime'];
                                     
                        var gettime = intakeTime;
                        var time = gettime.split(":");
                        var reminderdate = intakeDate.split("-");
                        var intakeAlarm = new Date(reminderdate[2],reminderdate[1]-1,reminderdate[0]);
                                     
                        intakeAlarm.setHours(time[0], time[1], 1);
                        var reminderDayTime = new Date(intakeAlarm);
                        var currentday = new Date();
                        if (reminderDayTime <= currentday)
                        {
                            intakes.getMedicationName(item['intakeID']);
                                     
                            cordova.plugins.notification.local.clearAll(function() {
                                //alert("verwijderd");
                            }, this);
                        }
                        else
                        {
                            //alert('reminder moet nog afgaan');
                        }
                    }
                }
            });
        });
    }
};

var feedback = {
    //START COPY PASTE

feedbackpage:function()
{
    $.mobile.changePage("#feedback-page", { transition: "pop", changeHash: true });
},

GetUpdatedPrescription:function(medicationID)
    {
        var query = "SELECT * FROM tblUpdatedPrescription WHERE medicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                resultset = result.rows;
                if(resultset.length == 0)
                {
                    $('#editOptions').append('<div class="editOptionsButton" onclick="feedback.removeMedicationRedirect(\''+medicationID+'\');" style="margin-right:0px; border-top-left-radius:5px; border-bottom-left-radius: 5px;"><div class="editButtonIcon"><i class="fa fa-times fa-2x"></i></div><div class="editButtonText"><strong>Gestopt?</strong><br /><small>Laat het ons weten</small></div><div class="clearer"></div></div>');
                                     
                    $('#editOptions').append('<div class="editOptionsButton" onclick="feedback.changePrescriptionRedirect(\''+medicationID+'\');" style="float: right; margin-left: 0px;border-top-right-radius:5px; border-bottom-right-radius: 5px;"><div class="editButtonIcon" style=" padding-top: 5px;"><center><i class="fa fa-edit fa-2x"></i></center></div><div class="editButtonText" style="padding-top: 0px;"><center><strong>Gebruik anders?</strong><br /><small>Laat het ons weten</small></center></div><div class="clearer"></div></div>');
                                     
                }
                else
                {
                    $('#newPrescription').show();
                    $('#prescriptionContainer').empty();
                    $('#prescriptionContainer').append('<div class="detailfield">Op advies van</div><div class="detailvalue">'+resultset.item(0)["adviesvan"]+'</div><div class="clearer"></div>');
                                     
                    $('#prescriptionContainer').append('<div class="detailfield">Reden</div><div class="detailvalue">'+resultset.item(0)["reden"]+'</div><div class="clearer"></div>');
                                     
                    $('#prescriptionContainer').append('<div class="detailfield">Aangepast naar</div><div class="detailvalue">'+resultset.item(0)["toelichting"]+'</div><div class="clearer"></div>');
                    
                    $('#editOptions').append('<div class="editOptionsButton" onclick="feedback.removeMedicationRedirect(\''+medicationID+'\');" style="margin-right:0px; border-top-left-radius:5px; border-bottom-left-radius: 5px;"><div class="editButtonIcon" style=" padding-top: 5px;"><center><i class="fa fa-times fa-2x"></i></center></div><div class="editButtonText" style="padding-top: 0px;"><center><strong>Gestopt?</strong><br /><small>Laat het ons weten</small><br /><br /></center></div><div class="clearer"></div></div>');
                          
                    $('#editOptions').append('<div class="editOptionsButton" onclick="feedback.removeUpdatedPrescription(\''+medicationID+'\');" style="float: right; margin-left: 0px;border-top-right-radius:5px; border-bottom-right-radius: 5px;"><div class="editButtonIcon" style=" padding-top: 5px;"><i class="fa fa-exclamation-triangle fa-2x"></i></div><div class="editButtonText" style="padding-top: 0px;"><strong>Verwijder</strong><br /><small>het aangepaste voorschrift</small></div><div class="clearer"></div></div>');
                                     
                    
                                     
                }
            });
        });
    },
    
removeUpdatedPrescription:function(medicationID)
    {
        //need to insert confirmation box.
        var deletequery = "DELETE FROM tblUpdatedPrescription WHERE medicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(deletequery,[],function(tx, result)
            {
                app.medicationDetails(medicationID);
            });
        });
    },
    
restoreMedication:function(medicationID)
    {
        //need to insert confirmation box.
        var deletequery = "DELETE FROM tblRemovedMedication WHERE medicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(deletequery,[],function(tx, result)
            {
                app.showMedication();
                        //window.plugin.notification.local.cancel(intakeID);
            });
        });
    },
    
removeMedicationRedirect:function(medicationID)
    {
        var query = "SELECT * FROM tblMedication WHERE medicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Er is een onverwachte fout opgetreden...", function(){}, "Melding", "OK");
                }
                else
                {
                    //alert(dataset.item(0)["medicationID"]);
                    $('#removeMedicationid').val(''+medicationID+'');
                    $('#removeMedicationName').val(dataset.item(0)["medicationName"]);
                    $.mobile.changePage("#removemedication-page", { transition: "pop", changeHash: true });
                }
            });
        });
    },
    
removeMedication:function()
    {
        if(adviesvan.value == "selecteer" || redenstoppen.value == "selecteer" || removeToelichtingArea.value == "")
        {
            navigator.notification.alert("Om uw apotheek op de hoogte te stellen dat u gestopt bent met dit medicijn dient u alle velden in te vullen.", function(){}, "Melding", "OK");
            //window.scrollTo(0,0);
            return;
        }
        else
        {
            var medication = ''+removeMedicationid.value+'';
            var medicationName = ''+removeMedicationName.value+'';
            var removeID = (((1+Math.random())*0x10000)|0).toString(16).substring(1);
            
            var insertRemovedMedication = "INSERT INTO tblRemovedMedication (removeID, medicationID, adviesvan, reden, toelichting) VALUES (?,?,?,?,?)";
            
            db.transaction(function(tx, results)
            {
                tx.executeSql(insertRemovedMedication,[removeID, medication, adviesvan.value, redenstoppen.value, removeToelichtingArea.value],dbase.succesCB,dbase.errorCB);
                           
                feedback.stoprecept('stoprecept',medication,medicationName,adviesvan.value, redenstoppen.value, removeToelichtingArea.value);
                           
                app.showMedication();
            });
        }
    },
    
changePrescriptionRedirect:function(medicationID)
    {
        var query = "SELECT * FROM tblMedication WHERE medicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Er is een onverwachte fout opgetreden.", function(){}, "Melding", "OK");
                }
                else
                {
                    $('#changeMedicationName').val(dataset.item(0)["medicationName"]);
                    $('#changeMedicationid').val(medicationID);
                                     
                    $.mobile.changePage("#updateprescription-page", { transition: "pop", changeHash: true });
                }
            });
        });
        
        $('#changeMedicationid').val(medicationID);
        $.mobile.changePage("#updateprescription-page", { transition: "pop", changeHash: true });
    },
    
changeMedicationPrescription:function()
    {
        if(changeAdviesvan.value == "selecteer" || changereden.value == "selecteer" || changeToelichtingArea.value == "")
        {
            navigator.notification.alert("Om uw apotheek op de hoogte te stellen dat u dit medicijn anders gebruikt dan voorgeschreven dient u alle velden in te vullen.", function(){}, "Melding", "OK");
            //window.scrollTo(0,0);
            return;
        }
        
        else
        {
            var medication = ''+changeMedicationid.value+'';
            var MedicationName = ''+changeMedicationName.value+'';
            var updatedID = (((1+Math.random())*0x10000)|0).toString(16).substring(1);
            
            var insertUpdatedMedication = "INSERT INTO tblUpdatedPrescription (updatedID, medicationID, adviesvan, reden, toelichting) VALUES (?,?,?,?,?)";
            
            db.transaction(function(tx, results)
            {
                tx.executeSql(insertUpdatedMedication,[updatedID, medication, changeAdviesvan.value, changereden.value, changeToelichtingArea.value],dbase.succesCB,dbase.errorCB);
                           
                feedback.changePrescription('wijzigrecept',medication,MedicationName,changeAdviesvan.value, changereden.value, changeToelichtingArea.value);
                           
                app.medicationDetails(changeMedicationid.value);
            });
        }
    },
    
stoprecept:function(subject,medicationID,medicationName,adviesvan,redenstoppen,removeToelichtingArea)
    {
        var body = 'Via de Apotheek app heeft deze patient aangegeven het medicijn '+medicationName+' ('+medicationID+') niet meer te gebruiken om onderstaande reden. \n Advies van: '+adviesvan+'. \n Reden van stoppen: '+redenstoppen+'. \n Toelichting van de patient: '+removeToelichtingArea+'.';
        
        
        //alert(body);
        var query = "SELECT agb, identifier, apiKey FROM tblPharmacy CROSS JOIN tblhhrApi";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Er is een onverwachte fout opgetreden..", function(){}, "Melding", "OK");
                }
                else
                {
                          
                    var agb = dataset.item(0)['agb'];
                    var apiKey = dataset.item(0)['apiKey'];
                    var identifier = dataset.item(0)['identifier'];
                    
                    if(identifier == null)
                    {
                        identifier = '';
                    }
                          
                    var appId = device.uuid+''+identifier;
                          
                    var deviceType = device.model;
                    
                    $.ajax({
                        url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                        type: 'POST',
                        data: { sendfeedback:agb, appid:appId, devicetype:deviceType, apikey:apiKey,subject:subject,body:body, medicationid:medicationID},
                        success: function(data) {
                           if(data == true)
                           {
                            navigator.notification.alert("Uw stop bericht is verzonden naar de apotheek.", function(){}, "Bericht", "OK");
                           }
                           else
                           {
                           //alert(data);
                            navigator.notification.alert("Er is een onverwachte fout opgetreden. Probeer het later nogmaals.", function(){}, "Melding!", "OK");
                           }
                        },
                        error: function()
                        {
                            navigator.notification.alert("Het is niet gelukt om de apotheek details op te halen", function(){}, "Melding", "OK");
                        }
                    });
                }
            });
        });
    },
    
changePrescription:function(subject, medication,MedicationName,changeAdviesvan,changereden,changeToelichtingArea)
    {

        var body = 'Via de Apotheek app heeft deze patient aangegeven het medicijn '+MedicationName+' ('+medication+') anders te gebruiken dan voorgeschreven om onderstaande reden. \n Advies van: '+changeAdviesvan+'. \n Reden van wijzigen: '+changereden+'. \n Nieuw voorschrift: '+changeToelichtingArea+'.';
        
        var query = "SELECT agb, identifier, apiKey FROM tblPharmacy CROSS JOIN tblhhrApi";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                          
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Er is een onverwachte fout opgetreden.", function(){}, "Melding", "OK");
                }
                else
                {
                    var agb = dataset.item(0)['agb'];
                    var apiKey = dataset.item(0)['apiKey'];
                    var identifier = dataset.item(0)['identifier'];
                                     
                    if(identifier == null)
                    {
                        identifier = '';
                    }
                                     
                    var appId = device.uuid+''+identifier;
                          
                    var deviceType = device.model;
                                     //var appId = device.uuid;
                                     
                    $.ajax({
                        url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                        type: 'POST',
                        data: { sendfeedback:agb, appid:appId, devicetype:deviceType, apikey:apiKey,subject:subject,body:body, medicationid:medication},
                        success: function(data) {
                        if(data == true)
                        {
                            navigator.notification.alert("Uw recept wijziging is verzonden naar de apotheek.", function(){}, "Bericht", "OK");
                        }
                        else
                        {
                            navigator.notification.alert("Er is een onverwachte fout opgetreden. Probeer het later nogmaals.", function(){}, "Melding!", "OK");
                        }
                        },
                        error: function()
                        {
                            navigator.notification.alert("Het is niet gelukt om de apotheek details op te halen", function(){}, "Melding", "OK");
                        }
                    });
                }
            });
        });
    },
    
addedMedication:function(addedMedication, addedSterkte, addedAantal, addedGebruik, addedVoorschrijver, addedStartdatum, addedEinddatum, addedOpmerkingen)
    {
        var body = 'Via de Apotheek app heeft deze patient onderstaand medicijn toegevoegd aan zijn/haar overzicht.\n Medicijn: '+addedMedication+' \n Sterkte: '+addedSterkte+'. \n Aantal: '+addedAantal+' \n Gebruik: '+addedGebruik+' \n Voorschrijver: '+addedVoorschrijver+' \n Startdatum: '+addedStartdatum+' \n Einddatum: '+addedEinddatum+' \n Opmerkingen: '+addedOpmerkingen;
        //alert(body);
        var medicationid = 'manual';
        
        var query = "SELECT agb, identifier, apiKey FROM tblPharmacy CROSS JOIN tblhhrApi";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                          
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Er is een onverwachte fout opgetreden.", function(){}, "Melding", "OK");
                }
                else
                {
                    var agb = dataset.item(0)['agb'];
                    var apiKey = dataset.item(0)['apiKey'];
                    var identifier = dataset.item(0)['identifier'];
                    var subject = 'Toegevoegde medicatie';
                    feedback.sendFeedback(agb, apiKey, identifier, subject, body, medicationid);
                }
            });
        });
    },
    
sendFeedback:function(agb, apiKey, identifier, subject, body, medication)
    {
        var deviceType = device.model;
        //var appId = device.uuid;
        if(identifier == null)
        {
            identifier = '';
        }
        
        var appId = device.uuid+''+identifier;
        
        $.ajax({
            url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
            type: 'POST',
            data: { sendfeedback:agb, appid:appId, devicetype:deviceType, apikey:apiKey,subject:subject,body:body, medicationid:medication},
            success: function(data)
            {
            if(data == true)
            {
               navigator.notification.alert("Uw medicatie toevoeging is verzonden naar de apotheek.", function(){}, "Bericht", "OK");
            }
            else
            {
               //alert(data);
               navigator.notification.alert("Er is een onverwachte fout opgetreden. Probeer het later nogmaals.", function(){}, "Melding!", "OK");
            }
            },
            error: function()
            {
               navigator.notification.alert("Er is een onverwachte fout opgetreden. Probeer het later nogmaals.", function(){}, "Melding", "OK");
            }
        });
    }

};

var add = {
    
addMedication:function()
    {
        if(addedMedication.value == "" || addedSterkte.value == "" || addedAantal.value == "" || addedGebruik.value == "" || addedVoorschrijver.value == "" || addedStartdatum.value == "")
        {
            navigator.notification.alert("U heeft nog niet alle verplichte velden van het formulier ingevuld.", function(){}, "Melding", "OK");
        }
        else
        {
            var addedMedicationID = (((1+Math.random())*0x10000)|0).toString(16).substring(1);
            
            var insertAddedMedication = "INSERT INTO tblAddedMedication (addedMedicationID, addedMedication, addedSterkte, addedAantal, addedGebruik, addedVoorschrijver, addedStartdatum, addedEinddatum, addedOpmerkingen, status) VALUES (?,?,?,?,?,?,?,?,?,?)";
            
            db.transaction(function(tx, results)
            {
                tx.executeSql(insertAddedMedication,[addedMedicationID, addedMedication.value,addedSterkte.value,addedAantal.value, addedGebruik.value, addedVoorschrijver.value, addedStartdatum.value, addedEinddatum.value, addedOpmerkingen.value,'active'],dbase.succesCB,dbase.errorCB);
                           
                feedback.addedMedication(addedMedication.value, addedSterkte.value, addedAantal.value, addedGebruik.value, addedVoorschrijver.value, addedStartdatum.value, addedEinddatum.value, addedOpmerkingen.value);
                           
                app.showMedication();

            });
        }
    },
    
checkForAddedMedication:function()
    {
        //alert('zoek en vind zelf toegevoegde medicatie.');
        var query = "SELECT * FROM tblAddedMedication WHERE status = 'active'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                if(dataset.length == 0)
                {
                    $('#addedMedicationList').empty();
                    //alert('geen zelf toegevoegde medicatie gevonden.');
                }
                else
                {
                    $('#addedMedicationList').empty();
                    $('#addedMedicationList').append($('<div class="addedMedicationDivider">Zelf toegevoegde medicatie</div>'));
                    
                    $('#addedMedicationList').append($('<ul data-role="listview" id="addedOverviewList">'));
                    $('#addedMedicationList').trigger('create');
                    
                    for (var i = 0, item =null; i < dataset.length; i++)
                    {
                        item = dataset.item(i);
                                     
                        $('ul#addedOverviewList').append('<li class="addedMedLi"><div id="'+item['addedMedicationID']+'" class="addedMedName" onclick="add.addedMedicationDetails(\''+item['addedMedicationID']+'\');">'+item['addedMedication']+'</div></li>').listview('refresh');
                        
                    }
                }
            });
        });
    },
    
addedMedicationDetails:function(medicationID)
    {
        var query = "SELECT * FROM tblAddedMedication WHERE addedMedicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                var start = new Date(dataset.item(0)["addedStartdatum"]);
                                     
                var startday = ('0' + start.getUTCDate()).slice(-2);
                var startmonth = start.getUTCMonth()+1;
                var startmonth = ('0' + startmonth).slice(-2);
                var startyear = start.getUTCFullYear();
                var startdate = startday+'-'+startmonth+'-'+startyear;
                                     
                var end = new Date(dataset.item(0)["addedEinddatum"]);
                                     
                var endday = ('0' + end.getUTCDate()).slice(-2);
                var endmonth = end.getUTCMonth()+1;
                var endmonth = ('0' + endmonth).slice(-2);
                var endyear = end.getUTCFullYear();
                var enddate = endday+'-'+endmonth+'-'+endyear;
                                     
                $('#addedDetailName').empty();
                $('#addedDetailName').append(dataset.item(0)["addedMedication"]);
                                     
                $('#addedDetailsContainer').empty();
                                     
                $('#addedDetailsContainer').append('<div class="detailfield">Sterkte</div><div class="detailvalue">'+dataset.item(0)["addedSterkte"]+'</div><div class="clearer"></div>');
                                     
                $('#addedDetailsContainer').append('<div class="detailfield">Aantal</div><div class="detailvalue">'+dataset.item(0)["addedAantal"]+'</div><div class="clearer"></div>');
                                     
                $('#addedDetailsContainer').append('<div class="detailfield">Startdatum</div><div class="detailvalue">'+startdate+'</div><div class="clearer"></div>');
                                     
                $('#addedDetailsContainer').append('<div class="detailfield">Einddatum</div><div class="detailvalue">'+enddate+'</div><div class="clearer"></div>');
                                     
                $('#addedDetailsContainer').append('<div class="detailfield">Voorschrijver</div><div class="detailvalue">'+dataset.item(0)["addedVoorschrijver"]+'</div><div class="clearer"></div>');
                                     
                $('#addedDetailsContainer').append('<div class="detailfield">Voorschrift</div><div class="detailvalue">'+dataset.item(0)["addedGebruik"]+'</div><div class="clearer"></div>');
                                     
                $('#addedDetailsContainer').append('<div class="detailfield">Opmerkingen</div><div class="detailvalue">'+dataset.item(0)["addedOpmerkingen"]+'</div><div class="clearer"></div>');
                                     
                $('#addedEditOptions').empty();
                                     
                $('#addedEditOptions').append('<div class="editOptionsButton" onclick="add.removeAddedMedication(\''+dataset.item(0)["addedMedicationID"]+'\');"><div class="editButtonIcon"><i class="fa fa-times fa-2x"></i></div><div class="editButtonText"><small>Verwijder dit medicijn.<br /></small></div><div class="clearer"></div></div>');
                                     
            });
        });
        
        $.mobile.changePage("#addedMedicationdetail-page", { transition: "slide", changeHash: true });
    },
    
removeAddedMedication:function(id)
    {
        //alert(id);
        //var deleteAddedMed = "DELETE FROM tblAddedMedication WHERE addedMedicationID = '"+id+"'";
        
        var deleteAddedMed = "UPDATE tblAddedMedication SET status = 'deleted' WHERE addedMedicationID = '"+id+"'";
        
        db.transaction(function(tx)
        {
            tx.executeSql(deleteAddedMed,[],function(tx, result)
            {
                //add.removePendingNotifications(id)
                app.showMedication();
                //window.plugin.notification.local.cancel(intakeID);
            });
        });
    },
    
showAddedMedicationName:function(medicationID, showType)
    {
        /**
         * reusable function for getting and parsing added medication name.
         **/
        var query = "SELECT * FROM tblAddedMedication WHERE addedMedicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
                       {
                       tx.executeSql(query,[],function(tx, result)
                                     {
                                     dataset = result.rows;
                                     if(dataset.length == 0)
                                     {
                                     $('#'+showType+'').empty();
                                     //$('#'+showType+'').append('zelf toegevoegde medicatie');
                                     }
                                     else
                                     {
                                     $('#'+showType+'').empty();
                                     $('#'+showType+'').append(dataset.item(0)["addedMedication"]);
                                     }
                                     });
                       });
    },
    
showAddedUsageText:function(medicationID, showType)
    {
        /**
         * reusable function for getting and parsing usage text name.
         **/
        var query = "SELECT * FROM tblAddedMedication WHERE addedMedicationID = '"+medicationID+"'";
        
        db.transaction(function(tx)
                       {
                       tx.executeSql(query,[],function(tx, result)
                                     {
                                     dataset = result.rows;
                                     if(dataset.length == 0)
                                     {
                                     $('#'+showType+'').empty();
                                     }
                                     else
                                     {
                                     $('#'+showType+'').empty();
                                     $('#'+showType+'').append('<div class="detailfield">Voorschrift</div><div class="detailvalue">'+dataset.item(0)["addedGebruik"]+'</div><div class="clearer"></div>');
                                     }
                                     });
                       });
    },
    
insertaddedMedicationName:function(medicationID, showType)
    {
        /**
         * reusable function for getting and parsing pharmacy name.
         **/
        var query = "SELECT * FROM tblAddedMedication WHERE addedMedicationID  = '"+medicationID+"'";
        
        db.transaction(function(tx)
                       {
                       tx.executeSql(query,[],function(tx, result)
                                     {
                                     dataset = result.rows;
                                     if(dataset.length == 0)
                                     {
                                     //alert('onbekende fout');
                                     }
                                     else
                                     {
                                     //$('#'+showType+'').empty();
                                     $('#'+showType+'').val(dataset.item(0)["addedMedication"]);
                                     }
                                     });
                       });
    },
};

var reset = {

logout:function()
    {
        //alert('en nu wat uitlog magie!');
        var deletetblPharmacy = "DELETE FROM tblPharmacy";
        var deletetblhhrApi = "DELETE FROM tblhhrApi";
        var deletetblPincode = "DELETE FROM tblPincode";
        var deleteMedication = "DELETE FROM tblMedication";
        var deleteTermsAndConditionsFlag = "DELETE FROM tblTermsAndConditions";
        
        db.transaction(function(tx, results)
        {
            tx.executeSql(deletetblPharmacy,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(deletetblhhrApi,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(deletetblPincode,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(deleteMedication,[],dbase.succesCB,dbase.errorCB);
            tx.executeSql(deleteTermsAndConditionsFlag,[],dbase.succesCB,dbase.errorCB);
                       
            navigator.notification.alert("U bent niet langer ingelogd in de Apotheek App.", function(){}, "Melding", "OK");
            
            //forceer om de terms-and-conditons weer te lezen
            $.mobile.changePage("#terms-and-conditions", { transition: "fade", changeHash: false });
        });
    }
    
};

var proactief = {
sendForm:function()
    {
        //var gender = $('input[name="proactiefgender"]:checked').val();
        var gender =proactiefgender.value;
        var voornaam =proactieffirstname.value;
        var achternaam = proactieflastname.value;
        var geboortedatum = proactiefbirthdate.value;
        var straat = proactiefstreet.value;
        var huisnummer = proactiefhousenumber.value;
        var huisnummertoevoeging = proactiefhousenumberaddition.value;
        var postcode = proactiefzipcode.value;
        var plaats = proactiefcity.value;
        var email = proactiefemailaddress.value;
        var mobiel = proactiefphonenumber.value;
        //alert(voornaam);
        
        if(voornaam == "" || achternaam == "" || geboortedatum == "" || straat == "" || huisnummer == "" || postcode == "" || plaats == "" || email == "" || mobiel == "")
        {
            $("#loadingRegistry").hide();
            navigator.notification.alert("Om u aan te melden voor de herhaalservice dient u alle velden in te vullen.", function(){}, "Melding", "OK");
        }
        
        else
        {
            var query = "SELECT * FROM tblPharmacy";
            
            db.transaction(function(tx)
            {
                tx.executeSql(query,[],function(tx, result)
                {
                    dataset = result.rows;
                    if(dataset.length == 0)
                    {
                        navigator.notification.alert("U heeft in de app nog niet aangegeven bij welke apotheek u klant bent.", function(){}, "Foutmelding", "OK");
                    }
                    else
                    {
                        var proactief = dataset.item(0)["agb"];
                        $.ajax({
                            url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                            type: 'POST',
                            data: { proactief:proactief, gender:gender, voornaam:voornaam, achternaam:achternaam, geboortedatum:geboortedatum, straat:straat, huisnummer:huisnummer, toevoeging:huisnummertoevoeging, postcode:postcode, plaats:plaats, email:email,  mobiel:mobiel},
                            success: function(data) {
                                //alert(data);
                               if(data == true)
                                {
                                    navigator.notification.alert("U heeft zich aangemeld voor de herhaalservice.", function(){}, "Aanmelding verzonden", "OK");
                                    $("#loadingRegistry").hide();
                                    $.mobile.changePage("#dashboard-page", { transition: "flip", changeHash: true });
                                }
                                else
                                {
                                    navigator.notification.alert("Er is een onverwachte fout opgetreden bij het het versturen van uw herhaalservice aanmelding.", function(){}, "Foutmelding", "OK");
                                }
                                                
                            },
                            error: function()
                            {
                                navigator.notification.alert("Er is een onverwachte fout opgetreden bij het het verzenden van uw herhaalservice aanmelding.", function(){}, "Helaas", "OK");
                            }
                        });
                    }
                });
            });
        }
    }
};

var pin = {
CodeExists:function()
    {
        var query2 = "SELECT * FROM tblhhrApi";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query2,[],function(tx, result)
            {
                dataset = result.rows;
                                     
                if(dataset.length == 0)
                {
                    //$.mobile.changePage("#login-page", { transition: "pop", changeHash: true });
                }
                else
                {
                    var query = "SELECT * FROM tblPincode";
                                     
                    db.transaction(function(tx)
                    {
                        tx.executeSql(query,[],function(tx, result)
                        {
                            dataset = result.rows;
                            if(dataset.length == 0)
                            {
                                $.mobile.changePage("#setpincode-page", { transition: "slideup", changeHash: true });
                            }
                            else
                            {
                                var diff = Math.abs(new Date(dataset.item(0)["enterdate"]) - new Date());
                                var minutes = Math.floor((diff/1000)/60);
                                
                                if(minutes <= 5)
                                {
                                    $.mobile.changePage("#medication-page", { transition: "pop", changeHash: true });
                                }
                                else
                                {
                                      $.mobile.changePage("#verifypincode-page", { transition: "slideup", changeHash: true });
                                      
                                }
                            }
                        });
                    });
                }
            });
        });
    },
    
savePincode:function(type)
    {
        var pw1 = document.getElementById(''+type+'pw1');
        var pw2 = document.getElementById(''+type+'pw2');
        var pw3 = document.getElementById(''+type+'pw3');
        var pw4 = document.getElementById(''+type+'pw4');
        var pw5 = document.getElementById(''+type+'pw5');
        var password = document.getElementById(''+type+'password');
        
        if(pw1.value == "" || pw2.value == "" || pw3.value == "" || pw4.value == "" || pw5.value == "")
        {
            navigator.notification.alert("U dient een 5 cijferige pincode in te stellen", function(){}, "Melding", "OK");
            $("#"+type+"password").val('');
        }
        else
        {
            
            var now             = new Date();
            //var password = ''+pw1.value+''+pw2.value+''+pw3.value+''+pw4.value+''+pw5.value;
            var password = document.getElementById(''+type+'password').value;
            
            var deletequery = "DELETE FROM tblPincode";
            var insertPincode = "INSERT INTO tblPincode (pincode, enterdate) VALUES (?,?)";
            
            db.transaction(function(tx, results)
            {
                tx.executeSql(deletequery,[],dbase.succesCB,dbase.errorCB);
                tx.executeSql(insertPincode,[password,now],dbase.succesCB,dbase.errorCB);
                           
                navigator.notification.alert("Uw pincode voor de app is ingesteld.", function(){}, "Melding", "OK");
                           $("#setPinform")[0].reset();
                           $("#"+type+"password").val('');
                if(type == 'first')
                {
                    $("#setPinform")[0].reset();
                    $.mobile.changePage("#medication-page", { transition: "pop", changeHash: true });
                }
                else
                {
                    $.mobile.changePage("#settings-page", { transition: "pop", changeHash: true });
                    $("#resetPinform")[0].reset();
                }
            });
        }
    },
    
VerifyPincode:function()
    {
        
        var verifypw1 = document.getElementById('verifypw1');
        var verifypw2 = document.getElementById('verifypw2');
        var verifypw3 = document.getElementById('verifypw3');
        var verifypw4 = document.getElementById('verifypw4');
        var verifypw5 = document.getElementById('verifypw5');
        
        if(verifypw1.value == "" || verifypw2.value == "" || verifypw3.value == "" || verifypw4.value == "" || verifypw5.value == "")
        {
            navigator.notification.alert("De door u ingevoerde pincode is onjuist, probeer het nogmaals", function(){}, "Melding", "OK");
            $("#verifyform")[0].reset();
        }
        else
        {
            var now             = new Date();
            var password = ''+verifypw1.value+''+verifypw2.value+''+verifypw3.value+''+verifypw4.value+''+verifypw5.value;

            var deletequery = "DELETE FROM tblPincode";
            var insertPincode = "INSERT INTO tblPincode (pincode, enterdate) VALUES (?,?)";
            
            var query = "SELECT pincode FROM tblPincode WHERE pincode = '"+password+"'";
            
            db.transaction(function(tx)
            {
                tx.executeSql(query,[],function(tx, result)
                {
                    dataset = result.rows;
                    if(dataset.length == 0)
                    {
                        navigator.notification.alert("De door u ingevoerde pincode is onjuist, probeer het nogmaals", function(){}, "Melding", "OK");
                                         $("#verifyform")[0].reset();
                    }
                    else
                    {
                        $("#verifyform")[0].reset();
                        $.mobile.changePage("#medication-page", { transition: "pop", changeHash: true });
                        
                        tx.executeSql(deletequery,[],dbase.succesCB,dbase.errorCB);
                        tx.executeSql(insertPincode,[password,now],dbase.succesCB,dbase.errorCB);
                        //dbase.checkForPharmacy();
                        //setup.checkNotifications();
                    }
                });
                
            });
        }
    },

validatePincode:function(type)
    {
        var now             = new Date();
        var pw1 = document.getElementById(''+type+'pw1');
        var pw2 = document.getElementById(''+type+'pw2');
        var pw3 = document.getElementById(''+type+'pw3');
        var pw4 = document.getElementById(''+type+'pw4');
        var pw5 = document.getElementById(''+type+'pw5');
        
        if(pw1.value == "" || pw2.value == "" || pw3.value == "" || pw4.value == "" || pw5.value == "")
        {
            navigator.notification.alert("U dient een 5 cijferige pincode in te voeren", function(){}, "Melding", "OK");
            $("#validatePinform")[0].reset();
            $("#"+type+"password").val('');
            
        }
        else
        {
            //var password = ''+pw1.value+''+pw2.value+''+pw3.value+''+pw4.value+''+pw5.value;
            var password = document.getElementById(''+type+'password').value;
            
            var query = "SELECT * FROM tblPincode WHERE pincode = '"+password+"'";
            var deletequery = "DELETE FROM tblPincode";
            var insertPincode = "INSERT INTO tblPincode (pincode, enterdate) VALUES (?,?)";
            
            db.transaction(function(tx)
            {
                tx.executeSql(query,[],function(tx, result)
                {
                    dataset = result.rows;
                    if(dataset.length == 0)
                    {
                        navigator.notification.alert("U heeft een ongeldige code ingevoerd, probeer het nogmaals", function(){}, "Melding", "OK");
                        $("#validatePinform")[0].reset();
                        $("#resetverifyPinform")[0].reset();
                              
                        $("#"+type+"password").val('');
                    }
                    else
                    {
                        tx.executeSql(deletequery,[],dbase.succesCB,dbase.errorCB);
                        tx.executeSql(insertPincode,[password,now],dbase.succesCB,dbase.errorCB);
                        if(type == "resetverify")
                        {
                              $("#"+type+"password").val('');
                              $("#resetverifyPinform")[0].reset();
                              $.mobile.changePage("#resetpincode-page", { transition: "pop", changeHash: true });
                        }
                        else
                        {
                              $("#"+type+"password").val('');
                              $("#validatePinform")[0].reset();
                              $.mobile.changePage("#medication-page", { transition: "pop", changeHash: true });
                        }
                    }
                });
            });
        }
    },
    
backtosettings:function()
    {
        //alert('aap');
        $("#changePin").show();
    },
    
};

var deals = {
retrieve:function()
    {
        $('#dealcontainer').empty();
        $.ajax({
               url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
               type: 'POST',
               data: { deals:'retrieve'},
               success: function(data) {
               //alert(data);
               if(data !== "")
               {
                    $('#dealcontainer').show();
                    $('#dealcontainer').append(data);
               }
               },
               error: function()
               {
               
               }
               });
        $('#dashboard-page').trigger('pagecreate');
        $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
    },
};

var appointments = {
appointmentDates:function()
    {
        var afspraakagb = document.getElementById('appointmentagb');
        var option = '<option value="selecteer">Selecteer een datum</option>';
        $('#appointmentDate').empty();
        $.ajax({
               url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
               type: 'POST',
               data: { getappointmentdates:afspraakagb.value},
               success: function(data) {
               //alert(data);
               option += data;
               $('#appointmentDate').append($('<label for="afspraakdatum" class="select"><strong>Selecteer een datum</strong></label><select name="afspraakdatum" id="afspraakdatum">'+option+'</select>'));
               
               $('#appointmentDate').trigger('create');
               
               },
               error: function()
               {
               navigator.notification.alert("Het is niet gelukt om beschikbare dagen voor een afspraak te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
               }
               });
        
        appointments.appointmentTimeslots(afspraakagb.value);
        $.mobile.changePage("#appointment-page", { transition: "pop", changeHash: true });
    },
    
appointmentTimeslots:function(agb)
    {
        var slots = '<option value="selecteer">Selecteer een tijdstip</option>';
        $('#appointmentTime').empty();
        $.ajax({
               url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
               type: 'POST',
               data: { getappointmenttimeslots:agb},
               success: function(data) {
               slots += data;
               $('#appointmentTime').append($('<label for="afspraaktijdstip" class="select"><strong>Selecteer een tijdstip</strong></label><select name="afspraaktijdstip" id="afspraaktijdstip">'+slots+'</select>'));
               
               $('#appointmentTime').trigger('create');
               
               },
               error: function()
               {
               navigator.notification.alert("Het is niet gelukt om beschikbare tijdstippen voor een afspraak te laden. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
               }
               });
        
    },
    
setAppointment:function()
    {
        var afspraakagb = document.getElementById('appointmentagb');
        var afspraakdatum = document.getElementById('afspraakdatum');
        var afspraaktijdstip = document.getElementById('afspraaktijdstip');
        var afspraaknaam = document.getElementById('appointmentnaam');
        var afspraaktel = document.getElementById('appointmenttel');
        var afspraakemail = document.getElementById('appointmentemail');
        
        if(!((afspraakdatum.value == 'selecteer') || (afspraaktijdstip.value == 'selecteer') || (afspraaknaam.value == '') || (afspraaktel.value == '') || (afspraakemail.value == '')))
        {
            $.ajax({
                   url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                   type: 'POST',
                   data: { appointment:afspraakagb.value, afspraakdatum:afspraakdatum.value, afspraaktijdstip:afspraaktijdstip.value, afspraaknaam:afspraaknaam.value, afspraaktel:afspraaktel.value, afspraakemail:afspraakemail.value},
                   success: function(data) {
                   if(data == true)
                   {
                   navigator.notification.alert("Uw afspraak verzoek is verzonden naar de apotheek.", function(){}, "Bevestiging", "OK");
                   $.mobile.changePage("#pharmacy-page", { transition: "slidedown", changeHash: true});
                   }
                   else
                   {
                   navigator.notification.alert("Het is niet gelukt om het afspraakverzoek door te sturen naar de apotheek. Probeer het later nogmaals.", function(){}, "Helaas", "OK");
                   }
                   },
                   error: function()
                   {
                   navigator.notification.alert("Het is niet gelukt om het afspraakverzoek door te sturen naar de apotheek.", function(){}, "Helaas", "OK");
                   }
                   });
            
        }
        else
        {
            navigator.notification.alert("U heeft nog niet alle velden van het formulier ingevuld.", function(){}, "Melding", "OK");
        }
    }

    
};

var recept = {
    checkIfReceptNumberIsAllowed:function()
    {
        var query = "SELECT * FROM tblPharmacy";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                        
                if(dataset.length == 0)
                {
                    //navigator.notification.alert("Onvoldoende apotheek gegevens beschikbaar.", function(){}, "Foutmelding", "OK");
                 
                }
                else
                {
                    $("#login-receptnumber-block").hide();
                    $("#medication-receptnumber-block").hide();

                    var agb = dataset.item(0)['agb'];
                    
                    $.ajax({
                        url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                        type: 'POST',
                        data: { checkreceptnumber:agb},
                        success: function(data) {
                            
                            if(data == 'yes')
                            {
                                $("#login-receptnumber-block").show();
                                $("#medication-receptnumber-block").show();
                            }
                        },
                        error: function()
                        {
                            navigator.notification.alert("Er is een onverwachte fout opgetreden bij het verzenden van uw receptnummer.", function(){}, "Helaas", "OK");
                        }
                    });
                }
            });
        });



        
    },

    getUserData:function(referer)
    {
        
        var query = "SELECT * FROM tblGegevens";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                                     
                if(dataset.length == 0)
                {
                    document.getElementById("receptnumberreferer").href="#"+referer;
                    $.mobile.changePage("#receptnumber-page", { transition: "pop", changeHash: true });
                }
                else
                {
                    
                    document.getElementById("receptnumberreferer").href="#"+referer;
                    
                    $("#recept-firstname").val(dataset.item(0)['firstname']);
                    $("#recept-prefix").val(dataset.item(0)['prefix']);
                    $("#recept-lastname").val(dataset.item(0)['lastname']);
                    $("#recept-birthdate").val(dataset.item(0)['birthdate']);
                    $("#recept-emailaddress").val(dataset.item(0)['emailaddress']);
                    $("#recept-phonenumber").val(dataset.item(0)['phonenumber']);
                         
                    $.mobile.changePage("#receptnumber-page", { transition: "pop", changeHash: true });
                }
            });
        });
    },

    takePicture:function()
    {
        var firstname = document.getElementById('recept-firstname').value;
        var prefix = document.getElementById('recept-prefix').value;
        var lastname = document.getElementById('recept-lastname').value;
        var birthdate = document.getElementById('recept-birthdate').value;
        var emailaddress = document.getElementById('recept-emailaddress').value;
        var phonenumber = document.getElementById('recept-phonenumber').value;
        var approveSending = document.getElementById('aprovesending').value;
        
        if(firstname == "" || lastname == "" || birthdate == "" || emailaddress == "" || phonenumber == "" )
        {
            navigator.notification.alert("Om een receptnummer door te sturen dient u alle verplichte velden in te vullen", function(){}, "Melding", "OK");
        }
        else if(approveSending == 'no')
        {
            navigator.notification.alert("U dient toestemming te geven voor het versturen van de bestelling", function(){}, "Melding", "OK");
        }
        else
        {
            webshop.insertPersonalData(firstname,prefix,lastname,birthdate,emailaddress,phonenumber);

            navigator.camera.getPicture(recept.onSuccess, recept.onFail, { quality: 25,
            destinationType: Camera.DestinationType.DATA_URL
            });
        }
    },
    onSuccess:function(imageData) 
    {
        var firstname = document.getElementById('recept-firstname').value;
        var prefix = document.getElementById('recept-prefix').value;
        var lastname = document.getElementById('recept-lastname').value;
        var birthdate = document.getElementById('recept-birthdate').value;
        var emailaddress = document.getElementById('recept-emailaddress').value;
        var phonenumber = document.getElementById('recept-phonenumber').value;

        var query = "SELECT * FROM tblPharmacy";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                          
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Onvoldoende apotheek gegevens beschikbaar om het receptnummer te verzenden.", function(){}, "Foutmelding", "OK");
                }
                else
                {
                    var agb = dataset.item(0)['agb'];

                    $.ajax({
                    url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                    type: 'POST',
                    data: { sendNumber:imageData, firstname:firstname, prefix:prefix, lastname:lastname, birthdate:birthdate, emailaddress:emailaddress, phonenumber:phonenumber, agb:agb},
                    success: function(data) {
                        $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
                        navigator.notification.alert(data, function(){}, "Verzonden", "OK");
                    },
                    error: function()
                    {
                        navigator.notification.alert("Er is een onverwachte fout opgetreden bij het verzenden van uw receptnummer.", function(){}, "Helaas", "OK");
                    }
                });                    
                }
            });
        });
    },

    onFail:function(message) 
    {
        //alert('Failed because: ' + message);
    },

};

var opinion = {
    checkIfFeedbackIsAllowed:function()
    {
        $(".flex-container").empty();
        var query = "SELECT * FROM tblPharmacy";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                    
                if(dataset.length == 0)
                {
                    //navigator.notification.alert("Onvoldoende apotheek gegevens beschikbaar.", function(){}, "Foutmelding", "OK");
                    $("#dashboard-feedback").hide();
                    $(".flex-container").append('<li class="flex-item"><a href="#webshop-page" data-transition="slideup"><i class="fa fa-shopping-cart fa-2x"></i><p>Webshop<br /><small>altijd 15% korting</small></p></a></li><li class="flex-item" onclick="app.showMedication();"><i class="fa fa-history fa-2x"></i><p>Mijn Medicatie<br /><small>Herhaalmedicatie bestellen</small></p></li><li class="flex-item" onclick="app.pharmacyDetails();"><i class="fa fa-medkit fa-2x"></i><p>Apotheek<br /><small>Informatie over uw apotheek</small></p></li><li class="flex-item" onclick="intakes.getIntakeHistory();"><i class="fa fa-calendar fa-2x"></i><p>Innameschema<br /><small>Overzicht innamen</small></p></li>');
                }
                else
                {
                    $("#dashboard-feedback").hide();
                    $("#dashboard-inschrijven").hide();
                    $("#dashboard-photorecept").hide();
                    var agb = dataset.item(0)['agb'];
                    checkagb = dataset.item(0)['agb'];
                    $.ajax({
                        url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
                        type: 'POST',
                        data: { checkfeedbackoption:agb},
                        success: function(data) {
                            var settings = JSON.parse(data);
                            $(".flex-container").append('<li class="flex-item"><a href="#webshop-page" data-transition="slideup"><i class="fa fa-shopping-cart fa-2x"></i><p>Webshop<br /><small>altijd 15% korting</small></p></a></li><li class="flex-item" onclick="app.showMedication();"><i class="fa fa-history fa-2x"></i><p>Mijn Medicatie<br /><small>Herhaalmedicatie bestellen</small></p></li><li class="flex-item" onclick="app.pharmacyDetails();"><i class="fa fa-medkit fa-2x"></i><p>Apotheek<br /><small>Informatie over uw apotheek</small></p></li>');
                            if(settings["receptnumber"] == 'yes')
                            {
                                $("#dashboard-photorecept").show();
                                $(".flex-container").append('<li class="flex-item" onclick="recept.getUserData(\'dashboard-page\');"><i class="fa fa-camera-retro fa-2x"></i><p>Recept via foto<br /><small>Makkelijk via een foto</small></p></li>');
                                
                            }
                            $(".flex-container").append('<li class="flex-item" onclick="intakes.getIntakeHistory();"><i class="fa fa-calendar fa-2x"></i><p>Innameschema<br /><small>Overzicht innamen</small></p></li>');
                            
                            if(settings["proactief"] == 'yes')
                            {
                                $(".flex-container").append('<li class="flex-item"><a href="#proactief-page"><i class="fa fa-refresh fa-2x"></i><p>Herhaalservice<br /><small>Wij verzorgen uw medicatie</small></p></li>');
                                
                            }

                            if(settings["subscribe"] == 'yes')
                            {
                                $(".flex-container").append('<li class="flex-item" onclick="subscribe.checkforpharmacy();"><i class="fa fa-pencil-square-o fa-2x"></i><p>Inschrijven<br /><small>Inschrijven bij de apotheek</small></p></li>');
                            }

                            if(settings["feedbackoption"] == 'yes')
                            {
                                $(".flex-container").append('<li class="flex-item" onclick="feedback.feedbackpage();"><i class="fa fa-comment fa-2x"></i><p>Opmerkingen<br /><small>Geef uw opmerkingen door</small></p></li>');
                            }
                            
                            if(settings["24uursservice"] == 'yes')
                            {
                                $(".flex-container").append('<li class="flex-item" onclick="twentyfourservice.open24subscribepage();"><i class="fa fa-clock-o fa-2x"></i><p>24-uurs service<br /><small>Gratis, snel en makkelijk</small></p></li>');
                            }
                            
                        },
                        error: function()
                        {
                            navigator.notification.alert("Er is een onverwachte fout opgetreden bij het ophalen van de instellingen.", function(){}, "Helaas", "OK");
                        }
                    });
                }
            });
        });
    },

    sendForm:function()
    {
        var feedback_textarea = document.getElementById("feedback_textarea").value;
        var feedback_name = document.getElementById("feedback_name").value;
        var feedback_phonenumber = document.getElementById("feedback_phonenumber").value;
        var feedback_email = document.getElementById("feedback_email").value;
        
        if (feedback_textarea ==""  )
        {
          navigator.notification.alert("U heeft nog geen opmerking of suggestie voor de Apotheek App ingevuld", function(){}, "Melding", "OK");
          return;
        }
        else
        {
          
          $.ajax({
            url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
            type: 'POST',
            data: { feedback:checkagb, suggestion:feedback_textarea, name:feedback_name, phonenumber:feedback_phonenumber, email:feedback_email},
            success: function(data) {
              $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
              navigator.notification.alert("Bedankt voor uw opmerking en/of suggestie", function(){}, "Verzonden", "OK");
            },
            error: function()
            {
            navigator.notification.alert("Er is een onverwachte fout opgetreden bij het versturen van uw feedback. Blijft dit probleem zich voordoen, neem dan contact op met apps@pharmeon.nl", function(){}, "Helaas", "OK");
            }
            });
        }
  }
    
};

var subscribe = {

    checkforpharmacy:function()
    {
        var query = "SELECT * FROM tblPharmacy";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                        
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Selecteer eerst een apotheek door te klikken op het groene vlak genaamd Apotheek.", function(){}, "Apotheek selecteren", "OK");
                }
                else
                {
                    subscribe.getInsuranceCompanies();
                    //implement check but for now just continue
                    $.mobile.changePage("#subscribe-page", { transition: "pop", changeHash: true });
                }
            });
        });
    },

    getInsuranceCompanies:function()
    {
        $.ajax({
            url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
            type: 'POST',
            data: { getInsuranceCompanies:'getList'},
            success: function(data) {
                $('#insuranceTypes').append($('<label for="subscribe_insurancecompany" class="select"><strong>Verzekeringsmaatschappij</strong></label><select name="subscribe_insurancecompany" id="subscribe_insurancecompany" data-mini="true">'+data+'</select>'));
                $('#insuranceTypes').trigger('create');
            },
            error: function()
            {
            navigator.notification.alert("Er is een onverwachte fout opgetreden bij het ophalen van de verzekeringsmaatschappijen. Blijft dit probleem zich voordoen, neem dan contact op met apps@pharmeon.nl", function(){}, "Helaas", "OK");
            }
            });
    },

    sendSubscription:function()
    {
        var agb = checkagb;
        var subscribe_gender = document.getElementById("subscribe_gender").value;
        var subscribe_lastname = document.getElementById("subscribe_lastname").value;
        var subscribe_initials = document.getElementById("subscribe_initials").value;
        var subscribe_firstname = document.getElementById("subscribe_firstname").value;
        var subscribe_birthdate = document.getElementById("subscribe_birthdate").value;
        var subscribe_street = document.getElementById("subscribe_street").value;
        var subscribe_housenumber = document.getElementById("subscribe_housenumber").value;
        var subscribe_zipcode = document.getElementById("subscribe_zipcode").value;
        var subscribe_city = document.getElementById("subscribe_city").value;
        var subscribe_phone = document.getElementById("subscribe_phone").value;
        var subscribe_email = document.getElementById("subscribe_email").value;
        //var subscribe_insurancecompany = document.getElementById("subscribe_insurancecompany").value;
        //var subscribe_polisnumber = document.getElementById("subscribe_polisnumber").value;
        //var subscribe_insurancestartdate = document.getElementById("subscribe_insurancestartdate").value;
        //var subscribe_generalpracticioner = document.getElementById("subscribe_generalpracticioner").value;
        var subscribe_lsp = document.getElementById("subscribe_lsp").value;
        var subscribe_medicationhistory = document.getElementById("subscribe_medicationhistory").value;
        var subscribe_textarea = document.getElementById("subscribe_textarea").value;

        if(agb == '')
        {
            navigator.notification.alert("U dient eerst een apotheek te selecteren.", function(){}, "Foutmelding", "OK");
        }
        else
        {
            $.ajax({
            url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback.php',
            type: 'POST',
            data: { sendSubscription:agb,gender:subscribe_gender,lastname:subscribe_lastname,initials:subscribe_initials,firstname:subscribe_firstname,birthdate:subscribe_birthdate,street:subscribe_street,housenumber:subscribe_housenumber,zipcode:subscribe_zipcode,city:subscribe_city,phone:subscribe_phone,email:subscribe_email,lsp:subscribe_lsp,medicationhistory:subscribe_medicationhistory,remarks:subscribe_textarea},
            success: function(data) {
                navigator.notification.alert("Uw inschrijving is verzonden naar de apotheek.", function(){}, "Verzonden", "OK");
                $.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
            },
            error: function()
            {
            navigator.notification.alert("Er is een onverwachte fout opgetreden bij het verzenden van uw inschrijving. Blijft dit probleem zich voordoen, neem dan contact op met apps@pharmeon.nl", function(){}, "Helaas", "OK");
            }
            });
        }
        

    }


};

var twentyfourservice = {
	open24subscribepage:function()
	{
		var query = "SELECT * FROM tblPharmacy";
        
        db.transaction(function(tx)
        {
            tx.executeSql(query,[],function(tx, result)
            {
                dataset = result.rows;
                        
                if(dataset.length == 0)
                {
                    navigator.notification.alert("Selecteer eerst een apotheek door te klikken op het groene vlak genaamd Apotheek.", function(){}, "Apotheek selecteren", "OK");
                }
                else
                {
                    $.mobile.changePage("#subscribe-24-page", { transition: "pop", changeHash: true });
                }
            });
        });
	},
	
	submit24subscribepage:function() {
		//gebruik hier (nog) geen sanitization. Client side is lastig en niet altijd te vertrouwen
		//In de php is het misschien wel een idee om de javascript er uit te filteren.
		var agb = checkagb;
        var subscribe_24_lastname = document.getElementById("subscribe_24_lastname").value;
        var subscribe_24_initials = document.getElementById("subscribe_24_initials").value;
        var subscribe_24_birthdate = document.getElementById("subscribe_24_birthdate").value;
        var subscribe_24_street = document.getElementById("subscribe_24_street").value;
        var subscribe_24_housenumber = document.getElementById("subscribe_24_housenumber").value;
        var subscribe_24_housenumber_addition = document.getElementById("subscribe_24_housenumber_addition").value;
        var subscribe_24_zipcode = document.getElementById("subscribe_24_zipcode").value;
        var subscribe_24_city = document.getElementById("subscribe_24_city").value;
        var subscribe_24_codes = document.getElementById("subscribe_24_codes").value;
        var subscribe_24_mobile = document.getElementById("subscribe_24_mobile").value;
        var subscribe_24_email = document.getElementById("subscribe_24_email").value;
        var subscribe_24_multi = document.getElementById("subscribe_24_multi").value;
        
        var subscribe_24_lastname_2 = subscribe_24_multi == 'Ja' ? document.getElementById("subscribe_24_lastname_2").value : '';
        var subscribe_24_initials_2 = subscribe_24_multi == 'Ja' ? document.getElementById("subscribe_24_initials_2").value : '';
        var subscribe_24_birthdate_2 = subscribe_24_multi == 'Ja'? document.getElementById("subscribe_24_birthdate_2").value: '';
 		
        var subscribe_24_lastname_3 = subscribe_24_multi == 'Ja' ? document.getElementById("subscribe_24_lastname_3").value: '';
        var subscribe_24_initials_3 = subscribe_24_multi == 'Ja' ? document.getElementById("subscribe_24_initials_3").value: '';
        var subscribe_24_birthdate_3 = subscribe_24_multi == 'Ja'? document.getElementById("subscribe_24_birthdate_3").value: '';
        
        var subscribe_24_lastname_4 = subscribe_24_multi == 'Ja' ? document.getElementById("subscribe_24_lastname_4").value: '';
        var subscribe_24_initials_4 = subscribe_24_multi == 'Ja' ? document.getElementById("subscribe_24_initials_4").value: '';
        var subscribe_24_birthdate_4 = subscribe_24_multi == 'Ja' ? document.getElementById("subscribe_24_birthdate_4").value: '';
        
        var subscribe_24_lastname_5 = subscribe_24_multi == 'Ja' ? document.getElementById("subscribe_24_lastname_5").value: '';
        var subscribe_24_initials_5 = subscribe_24_multi == 'Ja' ? document.getElementById("subscribe_24_initials_5").value: '';
        var subscribe_24_birthdate_5 = subscribe_24_multi == 'Ja' ? document.getElementById("subscribe_24_birthdate_5").value: '';

        if(agb == '')
        {
            navigator.notification.alert("U dient eerst een apotheek te selecteren.", function(){}, "Foutmelding", "OK");
        }
        else if (!(subscribe_24_lastname && subscribe_24_initials && subscribe_24_birthdate && subscribe_24_street && subscribe_24_housenumber && subscribe_24_zipcode && subscribe_24_city && subscribe_24_mobile && subscribe_24_email)) {
			if (!subscribe_24_lastname) {
				navigator.notification.alert("U dient uw achternaam in te vullen.", function(){}, "Achternaam ontbreekt", "OK");
				return;
			}
			if (!subscribe_24_initials) {
				navigator.notification.alert("U dient uw voorletter(s) in te vullen.", function(){}, "Voorletter(s) ontbreekt", "OK");
				return;
			}
			if (!subscribe_24_birthdate) {
				navigator.notification.alert("U dient uw geboortedatum in te vullen.", function(){}, "Geboortedatum ontbreekt", "OK");
				return;
			}
			if (!subscribe_24_street) {
				navigator.notification.alert("U dient uw straatnaam in te vullen.", function(){}, "Straatnaam ontbreekt", "OK");
				return;
			}
			if (!subscribe_24_housenumber) {
				navigator.notification.alert("U dient uw huisnummer in te vullen.", function(){}, "Huisnummer ontbreekt", "OK");
				return;
			}
			if (!subscribe_24_zipcode) {
				navigator.notification.alert("U dient uw postcode in te vullen.", function(){}, "Postcode ontbreekt", "OK");
				return;
			}
			if (!subscribe_24_city) {
				navigator.notification.alert("U dient uw woonplaats in te vullen.", function(){}, "Woonplaats ontbreekt", "OK");
				return;
			}			
			if (!subscribe_24_mobile) {
				navigator.notification.alert("U dient uw mobiel in te vullen.", function(){}, "Mobiel ontbreekt", "OK");
				return;
			}
			if (!subscribe_24_email) {
				navigator.notification.alert("U dient uw e-mailadres in te vullen.", function(){}, "E-mailadres ontbreekt", "OK");
				return;
			}																			
		}else
        {
            $.ajax({
				url: 'https://www.pharmeon-socialmedia.nl/apotheekapp/callback24uurs.php',
				type: 'POST',
				data: { 
					send24Subscription:agb,
					lastname:subscribe_24_lastname,
					initials:subscribe_24_initials,
					birthdate:subscribe_24_birthdate,
					street:subscribe_24_street,
					housenumber:subscribe_24_housenumber,
					housenumber_addition:subscribe_24_housenumber_addition,
					zipcode:subscribe_24_zipcode,
					city:subscribe_24_city,
					codes:subscribe_24_codes,
					mobile:subscribe_24_mobile,
					email:subscribe_24_email,
					multi:subscribe_24_multi,
					lastname_2:subscribe_24_lastname_2,
					initials_2:subscribe_24_initials_2,
					birthdate_2:subscribe_24_birthdate_2,
					lastname_3:subscribe_24_lastname_3,
					initials_3:subscribe_24_initials_3,
					birthdate_3:subscribe_24_birthdate_3,
					lastname_4:subscribe_24_lastname_4,
					initials_4:subscribe_24_initials_4,
					birthdate_4:subscribe_24_birthdate_4,
					lastname_5:subscribe_24_lastname_5,
					initials_5:subscribe_24_initials_5,
					birthdate_5:subscribe_24_birthdate_5															
				},
				success: function(data) {
					navigator.notification.alert(data, function(){}, "Verzonden", "OK");	
					twentyfourservice.resetAndGoToDashBoard();
				},
				error: function() {
					navigator.notification.alert('Er is een onverwachte fout opgetreden bij het verzenden van uw aanmelding. Blijft dit probleem zich voordoen, neem dan contact op met apps@pharmeon.nl.', function(){}, "Helaas", "OK");
				}
            });
        }	
			
	},
	 	
	resetAndGoToDashBoard:function() {	
		//reset van het formulier
		$(':input','#subscribe-24-page').val(''); //not(':button, :submit, :reset, :hidden').val('');
		$("#multi_persons").hide();
		$('#subscribe_24_multi').val("Nee");
		$('#subscribe_24_codes').val("Mobiel");
		$("#subscribe_24_multi").selectmenu('refresh', true);
		$('#subscribe-24-page select').selectmenu('refresh', true);
		
		//terug naar dashboard scherm
		$.mobile.changePage("#dashboard-page", { transition: "pop", changeHash: true });
	},
	
	change_24_multi:function(selectObject) {
		if (selectObject.value == "Ja") {
			$('#multi_persons').show();
		}
		else {
 			//input velden leeg maken
 			$(':input','#multi_persons').val('');
 			$('#multi_persons').hide();
		}	
	}	 
};

